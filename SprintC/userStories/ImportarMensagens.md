# User Story 4001 - Importar Mensagens Maquinas de ficheiros
=======================================


# 1. Requisitos

Como Serviço de comunicação com as máquinas pretendo que sejam importadas de forma concorrente/paralela, as mensagens existentes nos ficheiros de texto presentes no diretório de entrada de forma a disponibilizar as mesmas para processamento.

# 2. Análise

## Regras de Negócio

* Após importação, os ficheiros devem ser transferidos para um diretório de ficheiros processados.
* Os diretórios de entrada e de saida devem ser definidos por configuração aquando da implantação do sistema.
* Cada mensagen deve ser alvo de um processo de pré-validação, rejentando-a para log (diretório saida) se:
    * Mensagem com formato/estrutura desconhecida.
    * Mensagem com data futura (diferença de dias e não segundos).
    * Mensagem duplicada (ex. com mesmos atributos base - idMaquina, datahora, tipo de mensagem).
* Após importação e pré-validação cada mensagem deve ficar disponivel no sistema para posterior processamento.
* No mesmo diretório de entrada podem existir ficheiros com diferentes formatos e estruturas.

## Testes unitários
* idNotNull
* idNotEmpty
* tipoNotNull
* tipoNotEmpty
* dataNotNull
* dataNotFuture
* noRepeatedMensage


# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  MensagemBruta ?


**Controlador:**

* ImportarMensagensFicheiroController
* ImpoetarMensagensFicheiroService


**Repository:**

*  MensagemBrutaRepository


## 3.1. Realização da Funcionalidade

Após a inicialização do serviço o mesmo deve:
* Informar do seu início.
* Importar todos o ficheiros presentes no diretório de entrada (cada um referente a uma maquina);
* Ler cada mensagem presente em cada um dos ficheiros de forma paralela.
* Pré-validar cada mensagem com os critérios já referidos.
    * Efetuar log de erros. 
* Tranferir os ficheiros para o diretório de saida (ficheiros processados).
* Disponibilizar as mensagens para posterior processamento. 
* Informa do sucesso da operação 


## 3.2. SD / Diagrama de Classes

**SD**
![SD_ImportarMensagem](./SDImportarMensagensFicheiro.png)

**Diagrama de Classes**
![CD_ImportarMensagem](./CDImportarMensagensFicheiro.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Stratagy
* Padrão Factory


## 3.4. Testes 
* idNotNull
* idNotEmpty
* tipoNotNull
* tipoNotEmpty
* dataNotNull
* dataNotFuture
* noRepeatedMensage


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

Os directorios de entrada e de saida são definidos no application.properties

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

Por cada ficheiro lido do diretório de entrada é gerada uma thread para efetuar o processamento dos mesmos de forma paralela.



