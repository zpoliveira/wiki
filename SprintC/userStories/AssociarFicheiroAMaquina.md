# US3004 - Associar Ficheiro A Maquina
=========================================


# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo associar um ficheiro de configuração a uma máquina.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fabrica pode associar um ou mais ficheiros de configuração complementados com uma breve descrição. 

# 2. Análise

## Regras de Negócio

* Pode ser associado mais que um ficheiro de configuração a uma maquina;
* Cada associação de ficheiro de configuração tem de ser complementada com uma breve descrição.

## Testes unitários
* testFicheiroNotNull
* testFicheiroNotEmpty
* testDescricaoNotNull
* testDescricaoNotEmpty

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Maquina
*  FicheiroConfiguracao
*  DescricaoFicheiroConfiguracao

**Controlador:**

*  AssociarFicheiroConfiguracaoController

**Repository:**

* MaquinaRepository
* FicheiroConfiguracaoRepository 


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fabrica inicia o processo de associar um ficheiro de configuração a uma maquina
* O sistema apresenta uma lista de maquinas solicitando para selecionar a que pretende associar o ficheiro de configuração
* O sistema solicita a introdução de uma descrição do ficheiro de configuração
* O sistema solicita o upload do ficheiro de configuração
* O sistema apresenta mensagem de sucesso   
(esta enerente que a cada solicitação do sistema o Gestor de Chão de Fabrica introduz os dados solicitados)

## 3.2. SD / Diagrama de Classes

 

**SD**
![SD_AssociarFicheiroConfiguracaoMaquina](SDAssociarFicheiroConfiguracaoMaquina.png)


## 3.3. Padrões Aplicados

* Padrão Repository


## 3.4. Testes
* testFicheiroNotNull - é obrigatorio o ficheiro 
* testFicheiroNotEmpty - ficheiro nao pode estar vazio
* testDescricaoNotNull - é obrigatório descrição do ficheiro
* testDescricaoNotEmpty - descrição não pode ser vazia.


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

Para adição de vários ficheiros à mesma máquina o Gestor de Chão de Fabrica tem de executar este use case novamente (uma vez que nada foi solicitado em contrário).