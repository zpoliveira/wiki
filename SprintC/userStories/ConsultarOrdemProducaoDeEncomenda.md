# User Story 2012 - Consultar Ordens de Produção de uma encomenda

=========================================

# 1. Requisitos

Como Gestor de Produção, eu pretendo consultar as ordens de produção de uma dada encomenda.

A interpretação feita deste requisito foi no sentido de que qualquer o Gestor de Produção selecionaria uma encomenda existente no sistema para consultar as Ordens de Produção que lhe deram resposta.

# 2. Análise

## Regras de Negócio

-   A encomenda tem de existir no sistema.

## Testes unitários

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas.

**Classes do domínio:**

-   OrdemProducao
-   IdEncomenda

**Controlador:**

-   ConsultarOrdemProducaoDeEncomendaController

**Repository:**

-   OrdemProducaoRepository

## 3.1. Realização da Funcionalidade

-   O Gestor de Produção inicia processo de verificação de Ordens de produção existentes por encomenda
-   O sistema solicita introdução de identificador de encomenda
-   O Gestor de Produção insere o Id da Encomenda
-   O Sistema exibe Ordens de Produção originadas por esssa encomenda

## 3.2. SD / Diagrama de Classes

De forma semelhante a vários dos outros Diagramas de Sequência utilizados, neste também seria criado o Persistence Context, O Repository Factory e depois, a partir deste, os Repositories respetivos. Para facilidade de leitura, estes passos foram omitidos do SD que se segue.

**SD**
![SDConsultarOrdemProducaoDeEncomenda](SDConsultarOrdemProducaoDeEncomenda.png)

**Diagrama de Classes**
![CDConsultarOrdemProducaoDeEncomenda](CDConsultarOrdemProducaoDeEncomenda.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Visitor

## 3.4. Testes

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
