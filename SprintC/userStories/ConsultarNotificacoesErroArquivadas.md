# User Story 3007 - Consultar Notificações de Erro Arquivadas
=============================================================


# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo consultar uma ou mais notificações de erros de processamento arquivadas, tendo a possíbilidade de utilizar filtros para a sua seleção.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fábrica poderia utilizar os filtros  tipo de erro, linha de produção, máquina, data do erro, e outros dados associados à notificação do erro, de forma a selecionar notificações arquivadas e poder ver os seus detalhes. 

# 2. Análise

## Regras de Negócio

* Em cada momento, cada notificação tem de ter um, e só um, estado.
* Em cada momento, cada notificação tem de ser relativa a apenas um tipo de erro.
* As notificações têm de ter o estado Arquivado

## Testes unitários

* testeEstadoNotificacaoUnico
* testeEstadoNotificacaoAtivo
* testeErroNotificacaoNull
* testeErroNotificacaoUnico

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Notificação
*  Tipo de Erro

**Controlador:**

*  ConsultarNotificacaoArquivadaController

**Repository:**

*  NotificacaoRepository


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fábrica inicia o processo de arquivamento de notificações
* O sistema apresenta as opções de seleção de notificações arquivadas existentes (ou apresentar todas as notificações)
* O Gestor de Chão de Fábrica seleciona a opção desejada
* O Sistema apresenta as notificações que se enquadram no filtro utilizado     

## 3.2. SD / Diagrama de Classes.

**SD**
![SDConsultarNotificacaoArquivada](SDConsultarNotificacaoArquivada.png)

**Diagrama de Classes**
![CDConsultarNotificacaoArquivada](CDConsultarNotificacaoArquivada.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Visitor (para impressão das notificações)

## 3.4. Testes 

* testeEstadoNotificacaoUnico - Verificar que não é possível uma notificação de erro ter o campo estado em ativo e arquivado simultâneamente
* testeEstadoNotificacaoAtivo - Verificar que quando uma notificação de erro é instânciada o estado é por defeito ativo
* testeErroNotificacaoNull - Verificar que não é possível uma notificação de erro ter o campo tipoErro a null
* testeErroNotificacaoUnico - Verificar que não é possível uma notificação de erro ter o campo tipoErro com mais que um tipo de erro

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*