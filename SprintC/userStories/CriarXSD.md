# User Story 1010 - Criação de XSD
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que a equipa especifique uma documento XSD que possa ser, posteriormente, usado para validar o conteúdo XML gerado pelo sistema.
O XSD deve contemplar toda a infomação subjacente ao chão de fábrica (e.g. produtos, matérias-primas, máquinas, linhas de produção, categorias, ordens de produção, fichas de produção, lotes, consumos reais e efetivos, estornos, desvios, tempos de produção,  entre outros).

# 2. Análise

* Utilização da versão 1.1
* Encoding UTF-8



## Testes


# 3. Design

* Divisão do XSD por area funcionais da fabrica e seguidamente para agregados maioritariamente( utilização de uma secção que contem validações que serão utilizadas mais que uma vez) que serão importados para um main.xsd 

![XSD_Schema](XSD_Schema.png)

## 3.1. Realização da Funcionalidade



## 3.4. Testes 



# 4. Implementação



# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações





