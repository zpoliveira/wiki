# User Story 2007 - Exprtar XML de Fabrica

## 1. Requisitos

Como utilizador da aplicação pretendo exportar toda a representação da informação de chão de fábrica e respetiva produção para um ficheiro XML.

## 2. Análise

### Regras de Negócio

-   Toda a informaçao deve ser exportada para o formato pretendido.
-   Deve ser usado um ficheiro de schema para validação do fiicheiro final.

### Testes unitários

## 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

Serão usadas todas as classes de domínio, e exportada toda a informação relevante.

**Controlador:**

-   ExportarFabricaController

**Builder:**

- XmlFabricaBuilder;


### 3.1. Realização da Funcionalidade

-   O Utilizador inicia o processo de boootstrap de depósitos
-   O sistema apresenta o nome dos depósitos carregados e mensagem de sucesso

### 3.2. SD / Diagrama de Classes

**SD**
![SD_BootDepositos](SD_BootDepositos.png)

**Diagrama de Classes**
![CD_BootDepositos](CD_BootDepositos.png)

### 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Factory
-   Padrão Command (no menu de bootstrap)

### 3.4. Testes

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

    @Test(expected = IllegalArgumentException.class)
    	public void ensureNullIsNotAllowed() {
    	Exemplo instance = new Exemplo(null, null);
    }

## 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

## 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

## 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
