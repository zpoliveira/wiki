# User Story 4002 - Recolher Mensagens de Maquina
=======================================


# 1. Requisitos

Como Serviço de comunicação com as máquinas pretendo que sejam recolhidas menssagens provenientes das máquinas de uma determinada linha de produção, de forma a disponibilizar as mesmas para processamento.

# 2. Análise

## Regras de Negócio

* Deve ser criado um log de todas as mensagens recebidas das máquinas;
* Reconhecer as mensagens segundo o protocolo de comunicação;
* O sistema central deve conhecer o número de identificação único de cada máquina industrial através do seu repositório de dados.
* Cada mensagen deve ser alvo de um processo de pré-validação, rejentando-a para log (diretório saida) se:
    * Mensagem com formato/estrutura desconhecida.
    * Mensagem com data futura (diferença de dias e não segundos).
    * Mensagem duplicada (ex. com mesmos atributos base - idMaquina, datahora, tipo de mensagem).
* Após importação e pré-validação cada mensagem deve ficar disponivel no sistema para posterior processamento.


## Testes unitários

* idNotNull
* idNotEmpty
* tipoNotNull
* tipoNotEmpty
* dataNotNull
* dataNotFuture
* noRepeatedMensage
* idProtocolNotNull
* idProtocolNotEmpty


# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

Classes relacionadas mas que não fazem parte do dominio de negócio mas que são importantes para este caso uso:

* ScmSocketServer
* ScmSocketServerThread 
* MensagemBruta


**Controlador:**

* ScmSocketController
* MensagemBrutaController


**Repository:**

* MensagemBrutaRepository


## 3.1. Realização da Funcionalidade

* O Serviço de comunicação com as máquinas é iniciado ficando num endereço e porta especifica;
* Na sua inicialização é inicializado um socket server que fica a escuta de mensagens dos clientes;
* Por cada cliente connectado é gerada uma thread com a sua conecção (a conecção do cliente é efetuada como especifcado no protocolo);
* Após conecção bem sucedida por cada mensagem com "raw data" são aplicados os critérios de pré-validação já descritos;
* Após pré-validação é persistida a mensagem para posterior processamento;
* A cada mensagem recebida é sempre enviado um ACK ou NACK para o cliente.


## 3.2. SD / Diagrama de Classes

**SD**
![SD_RecolherMensagens]()

**Diagrama de Classes**
![CD_RecolherMensagens]()

## 3.3. Padrões Aplicados

* 

## 3.4. Testes 



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*
O serviço inicia e contacta o ScmSocketController através do método startSocketServer(), por sua vez o ScmSocketServer fica a escuta de conecções sendo que para cada uma é gerada um thread ScmSocketServerThread a qual recebe os dados provenientes do simulador de maquina e envia a resposta para o mesmo. Com a mensagem de hello é verificado se o idMaquina presente no protocolo é conhecido no sistema se sim é armazenado o id e o endereço de rede de forma a que nas seguintes mensagens não seja necessário efetuar pedidos novamente à base daddos para verificar se maquina existe no sistema uma vez que já foi validado.
No caso do id da maquina não ser conhecido a resposta será um NACK, contudo toda e qualquer pre-validação das mensagens é repondido um ACK de forma acontinuar a receber as mensagens mas se existirem erros de pré-validação estas são guardadas no ficheiro de log.

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



