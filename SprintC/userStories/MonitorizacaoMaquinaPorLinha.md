# User Story 6001 - SMM
=======================================


# 1. Requisitos

Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.

# 2. Análise

## Regras de Negócio

A comunicação entre o sistema de monitorização e as máquinas é baseada em UDP e assenta nos seguintes pressupostos:
 * Não há conhecimento mútuo inicial;
 * As máquinas industriais tornam-se conhecidas pelo sistema de monitorização ao responder a
pedidos HELLO remetidos por este;
 * As máquinas industriais nunca enviam pedidos ao sistema de monitorização.
O sistema de monitorização envia pedidos HELLO baseados em UDP para máquinas industriais. Neste caso específico, nestes pedidos não consta o número de identificação único da máquina e, portanto, deve ser zero. A resposta ao pedido HELLO é ACK ou NACK, dependendo da última resposta que a máquina industrial recebeu do sistema central. Qualquer texto de status retornado pelo sistema central na última resposta deve ser adicionado à resposta enviada ao sistema de monitorização.
O sistema de monitorização deve possuir uma lista de endereços de rede a serem monitorizados, podendo esta lista incluir endereços específicos (unicast) e/ou de difusão (broadcast). A cada trinta segundos (ou outro valor configurado), o sistema de monitorização envia um pedido HELLO baseado em UDP para cada um desses endereços. A partir das respostas que chegam ao sistema de monitorização, uma lista de máquinas industriais é construída e mantida. Se não houver atualização de uma máquina industrial por mais de um minuto (valor configurável), esta deverá ser marcada como indisponível, mas não removida da lista.


## Testes unitários


# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

Classes relacionadas mas que não fazem parte do dominio de negócio mas que são importantes para este caso uso:

* EstadoMaquina
* Mensagem 
* TipoMensagem


**Controlador:**

* DataValidator
* UdpSender
* UdpStart


**Repository:**

* MensagemBrutaRepository


## 3.1. Realização da Funcionalidade

* O Serviço de monotirizaçao com as máquinas é iniciado e porta especifica;
* Na sua inicialização é inicializado um socket client que fica mediante uma lista de ip vai fazer pedidos Hello para o Sevido UDP nas máquianas, de forma repetida(x em x tempo);
* Os serviores UPD que vão estar á escuta var enviar uma Resposta ACK ou NACK;
* Após receber uma resposta o ip e a resposta da máquina e gruardada na memória.


## 3.2. SD / Diagrama de Classes


## 3.3. Padrões Aplicados

* 

## 3.4. Testes 



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



