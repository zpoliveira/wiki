# User Story 1012 - Pedido de monitorização do estado da máquina
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.

# 2. Análise

## Regras de Negócio
O sistema de monitorização envia pedidos HELLO baseados em UDP para máquinas industriais. Neste caso específico, nestes pedidos não consta o número de identificação único da máquina e, portanto, deve ser zero. A resposta ao pedido HELLO é ACK ou NACK, dependendo da última resposta que a máquina industrial recebeu do sistema central. Qualquer texto de status retornado pelo sistema central na última resposta deve ser adicionado à resposta enviada ao sistema de monitorização.
O sistema de monitorização deve possuir uma lista de endereços de rede a serem monitorizados, podendo esta lista incluir endereços específicos (unicast) e/ou de difusão (broadcast). A cada trinta segundos (ou outro valor configurado), o sistema de monitorização envia um pedido HELLO baseado em UDP para cada um desses endereços. A partir das respostas que chegam ao sistema de monitorização, uma lista de máquinas industriais é construída e mantida. Se não houver atualização de uma máquina industrial por mais de um minuto (valor configurável), esta deverá ser marcada como indisponível, mas não removida da lista.

## Testes unitários


# 3. Design



## 3.1. Realização da Funcionalidade

 * Servidor UDP recebe uma pedido Hello
 * Var ler na memória partilhada a ultima resposta do Cliente de TCP
 * Prepara a mensagem e envia para o servidor que enviou o pedido 



## 3.2. SD / Diagrama de Classes



## 3.3. Padrões Aplicados


## 3.4. Testes 


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



