# User Story 3006 - Arquivar Notificações de Erro
=================================================


# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo arquivar uma ou mais notificações de erros de processamento.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fábrica poderia verificar que notificações de erro ainda se encontravam por arquivar, selecionando uma ou mais para arquivamento.  

# 2. Análise

## Regras de Negócio

* Em cada momento, cada notificação tem de ter um, e só um, estado.
* Em cada momento, cada notificação tem de ser relativa a apenas um tipo de erro.
* A notificação é criada no estado Ativo

## Testes unitários

* testeEstadoNotificacaoUnico
* testeEstadoNotificacaoAtivo
* testeErroNotificacaoNull
* testeErroNotificacaoUnico

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Notificação
*  Tipo de Erro

**Controlador:**

*  ArquivarNotificacaoController

**Repository:**

*  NotificacaoRepository


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fábrica inicia o processo de arquivamento de notificações
* O sistema apresenta as notificações existentes (e os seus detalhes) por arquivar
* O Gestor de Chão de Fábrica seleciona as notificações pretendidas, uma a uma, até pressionar o zero
* O Sistema apresenta mensagem de sucesso e informa de quantas notificações foram arquivadas     


## 3.2. SD / Diagrama de Classes

**SD**
![SDArquivarNotificacaoErro](SDArquivarNotificacaoErro.png)

**Diagrama de Classes**
![CDArquivarNotificacaoErro](CDArquivarNotificacaoErro.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Visitor (para impressão das notificações)

## 3.4. Testes 

* testeEstadoNotificacaoUnico - Verificar que não é possível uma notificação de erro ter o campo estado em ativo e arquivado simultâneamente
* testeEstadoNotificacaoAtivo - Verificar que quando uma notificação de erro é instânciada o estado é por defeito ativo
* testeErroNotificacaoNull - Verificar que não é possível uma notificação de erro ter o campo tipoErro a null
* testeErroNotificacaoUnico - Verificar que não é possível uma notificação de erro ter o campo tipoErro com mais que um tipo de erro

# 4. Implementação

* A equipa entendeu considerar o estado da notificação (activa/arquivada) como um boolean, por conseguir desta forma representar o comportamento do estado. caso surjam mais estados, ou haja necessidade de comportamentos mais elaborados, este boolean será "promovido" a uma classe de modo a conseguir-se assegurar os comportamentos que sejam necessários.

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*