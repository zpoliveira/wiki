# User Story 2010 - Adicionar Ordem Produção Manualmente

=================================================

# 1. Requisitos

Como Gestor de Produção, eu pretendo introduzir manualmente uma nova ordem de produção.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção teria a possibilidade de acrescentar, manualmente, uma nova Ordem de Produção.

# 2. Análise

## Regras de Negócio

-	A Ordem de Produção tem um código único interno; 
-   O Produto tem de existir no sistema;
-   Se o produto ainda não dispõe de uma Ficha de Produção, o sistema deve avisar o utilizador mas deve permitir criar a ordem de produção;
-   A Ordem de Produção é sempre criada com o estado "Pendente";
-   A data prevista de execução deve ser igual ou posterior à data de emissão da ordem de produção.

## Testes unitários

-   testDataEmissaoNotNull;
-	testDataPrevistaExecucaoNotNull;
-	testCodigoFabricoProdutoNotNull;
-	testCodigoFabricoProdutoNotEmpty;
-	testEncomendaIdNotNull;
-	testEncomendaIdNotEmpty;
-	testEstadoOrdemProducaoPendente; 
# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   OrdemProducao
-   IdentificadorOrdemProducao
-	CodigoFabricoProduto
-	DataEmissao
-	DataPrevistaExecucao
-	EncomendaId
-	EstadoOrdemProducao

**Controlador:**

-   AdicionarOrdemProducaoController

**Repository:**

-   OrdemProducaoRepository

## 3.1. Realização da Funcionalidade

-   O Gestor de Produção solicita a especificação de uma nova Ordem de Produção
-   O sistema solicita a introdução dos dados necessários (IdentificadorOrdemProducao, CodigoFabricoProduto, DataEmissao, DataPrevistaExecucao e EncomendaId)
-   O Gestor de Produção insere os dados
-   O sistema valida os dados e informa o Gestor de Produção do sucesso da operação
-  	O caso de uso termina

## 3.2. SD / Diagrama de Classes

De forma semelhante a vários dos outros Diagramas de Sequência utilizados, neste também seria criado o Persistence Context, O Repository Factory e depois, a partir deste, os Repositories respetivos. Para facilidade de leitura, estes passos foram omitidos do SD que se segue.

**SD**
![SDAdicionarOrdemProducao](SDAdicionarOrdemProducao.png)

**Diagrama de Classes**
![CDAdicionarOrdemProducao](CDAdicionarOrdemProducao.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Factory
-   Padrão Visitor
-   Padrão Commander

## 3.4. Testes

**testDataEmissaoNotNull:** Verificar que não é possível criar uma Ordem de Produção com data de emissão a null.
**testDataEmissaoNotEmpty:** Verificar que não é possível criar uma Ordem de Produção sem data de emissão.
**testDataPrevistaExecucaoNotNull/Empty:** Data prevista de execução não pode ser null ou vazio.
**testCodigoFabricoProdutoNotNull/Empty:** Codigo de fabrico do produto não pode ser null ou vazio.
**testEncomendaIdNotNull/Empty:** ID da encomenda não pode ser null ou vazio.
**testEstadoOrdemProducaoPendente:** Validação que as Ordens de Produção são criadas com estado "Pendente".

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
