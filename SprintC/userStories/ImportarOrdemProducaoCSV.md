# User Story 2005 - Importar Produtos CSV

=================================================

# 1. Requisitos

Como Gestor de Produção, eu pretendo importar um ficheiro CSV com uma lista de Produtos para adicionar ao sistema.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção teria a possibilidade de acrescentar uma lista deprodutos disponivel em ficheiro CSV,

# 2. Análise

## Regras de Negócio

-   O Produto sem Ficha de Produção tem de existir no sistema;
-   Cada Produto é único;
-   Não sendo possivel importar um dado produto, deve ser registada a falha;

## Testes unitários

-   testFicheiroNull;
-   testFicheiroEmpty;

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   Produto

**Controlador:**

-   ImportarCatalogoController

**Repository:**

-   ProdutoRepository

## 3.1. Realização da Funcionalidade

-   O Gestor de Produção inicia o processo de importação de ficheiro
-   O sistema pede a localização do ficheiro
-   O Gestor de Produção indica o caminho
-   O sistema localza e valida o ficheiro
-   O sistema importa todas os produtos existentes no ficheiro, registando as falhas de importação num ficheiro proprio

## 3.2. SD / Diagrama de Classes

**SD**
![SD_ImportarCatalogo](SD_ImportarCatalogoCSV.png)

**Diagrama de Classes**
![CD_ImportarCatalogo.png](CD_ImportarCatalogoCSV.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Strategy

## 3.4. Testes

**testFicheiroNull:** Verificar que não é possível importar um ficheiro inexistente ou nulo.

**testFicheiroEmpty:** Verificar que não é possível importar um ficheiro sem conteudo, ou com conteudo imcompleto.

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
