# User Story 5001 - Efetuar Processamento de Mensagens
# 1. Requisitos

Como Serviço de Processamento de Mensagens (SPM), pretendo efetuar o processamento das mensagens disponiveis no sistema.

A interpretação feita deste requisito foi no sentido de que o SPM conseguiria, neste momento, recolher e processar as mensagens, dentro de um período definido de tempo, que tinham sido previamente persistidas. Este processamento teria de ser realizado de forma independente para cada linha de produção. O SPM deve ser capaz de realizar a Validação e Enriquecimento das mensagens, bem como obter, após o processamento das mensagens, para cada Ordem de Produção, dados relativos a: (i) se a mesma já se encontra (ou não) em execução; (ii) quando é que se iniciou e concluiu a sua execução; (iii) a linha de produção e as máquinas onde a mesma decorreu; (iv) tempo bruto de execução (i.e. todo o tempo decorrido) e tempo efetivo de execução (i.e. não considera paragens devido a falhas ocorridas); (v) detalhe de tempos (brutos e efetivos) por máquina; (vi) os consumos reais das matérias-primas envolvidas na produção do produto em causa bem como potenciais desvios relativamente à lista de matérias-primas constante na sua ficha de produção (bill of materials); (vii) os lotes e respetivas quantidades de produto resultantes.

# 2. Análise

## Regras de Negócio

* Não podem existir mensagens repetidas;
* O SPM não pode processar mensagens correspondentes a uma linha que não tenha sido selecionada;
* O SPM processa as mensagens de todas as Linhas de Produção quando nenhuma é selecionada;
* Tem de ser introduzido pelo menos o início do período a analisar;
* A Ordem de Produção tem de existir;
* O Estado da Ordem de Produção tem de ser atualizada como consequência do SPM;
* A Execução de Ordem de Produção tem de ser atualizada como consequência do SPM;
* Erros ocorridos durante este processo devem gerar notificações.

## Testes unitários

Testes realizados aos elementos do dominio;
* testeIdentificadorordemProducaoNotNull
* testeDataHoraNotNull
* testeCodigoInternoMaquinaNotNull
* testeIdentificadorordemProducaoNotNull
* testeLinhaProducaoIdentificadorNotNull
* testeCondigoInternoMateriaPrimaNotNull
* testeCodigoAlfanumericoNotNull
* testeQuantidadeNotNull
* testeCodigoFabricoProdutoNotNull
* testeLoteIdNotNull
* testeLoteIdNotEmpty
* testeCodigoParagemNotNull
* testeCodigoParagemNotEmpty

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  ExecucaoOrdemProducao
*  IdentificadorOrdemProducao
*  ConsumoRealMateriaPrima
*  DesvioConsumoMateriaPrima
*  DesvioProducao
*  ProducaoReal
*  TempoEfetivoExecucao
*  TempoBrutoExecucao
*  TempoExecucaoMaquina
*  CodigoInternoMaquina
*  Quantidade
*  Mensagem  

**Controlador:**

*  EnriquecimentoService
*  ProcessamentoController
*  ProcessamentoService

**Repository:**

*  ExecuçãoOrdemProducaoRepository
*  OrdemProducaoRepository
*  LinhasProducaoRepository
*  MensagemBrutaRepository
*  MensagemRepository


## 3.1. Realização da Funcionalidade

* Inicia-se o SPM com especificação obrigatória do início do período de processamento e opcionalmente do fim deste período e da(s) Linhas de Produção a processar
* O SPM processa as mensagens existentes nesse período e atualiza os dados da Execução da Ordem de Produção assim como o Estado da Ordem de Produção;
* Informa do sucesso da operação.  


## 3.2. SD / Diagrama de Classes

**SD**
* A demonstração deste US evidencia-se pela sua descrição, uma vez que o seu funcionamento é essencialmente a realização de cálculos; 

**Diagrama de Classes**


## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory

## 3.4. Testes 


# 4. Implementação


* O SPM recolhe mensagens já pré-validadas, iniciando o seu processamento pelo seu enriquecimento;
* Foram definidos três tipos de erros de processamento: Dados inválidos (identificadores errados, por exemplo), Elementos não específicados no sistema (por exemplo, Ordem de produção inexistente) e Paragem forçada de máquina (à qual está associada um código de motivo de paragem). Outros tipos de erro podem ser acrescentados, em função de existir essa necessidade;
* Têm de ser geradas notificações de acordo com os tipos de erros supracitados.

# 5. Integração/Demonstração

* O SPM integra quer com as mensagens recolhidas pelo SCM (e persistidas em repositório), quer com a entidade Notificações (sempre que seja detectado algum erro), quer com a entidade ExecucaoOrdemProducao, que irá persitir os detalhes obtidos pelo processamento de mensagens, relacionados com cada Ordem de produção.

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*




