# LAPR4 2019-2020 Project - Sprint C planning -> RCOMP spint 4

### Sprint master: 1040321

(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog

In this spint three applications will be developed:
- **Central System (Sistema Central)** 
- **Industrial Machine agent emulator** (Máquinas Industriais ou Emuladores);
- **Monitoring System with a WEB interface** (Sistema de Monitorização);

The Central System communicates with Industrial Machines, in turn, Industrial Machines communicate with
the Monitoring System, finally, the Monitoring System has a WEB interface, and thus it will communicate
with WEB Browsers. The five network communications related user stories are about:

# 2. Technical decisions and coordination

1. Development of the Central System application features regarding communications with Industrial Machines. -> 4002 Como SCM, pretendo proceder à recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção.


2. Development of the Industrial Machines application regarding communications with the Central System. -> 1011 Como Gestor de Projeto, eu pretendo que a equipa desenvolva uma aplicação que simule o funcionamento de uma máquina, nomeadamente no envio de mensagens geradas por estas.


3. Development of the Industrial Machines application regarding communications with the Monitoring System. -> 1012 Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.


4. Development of the Monitoring System application regarding communications with Industrial Machines. -> 6001 Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.


5. Development of the Monitoring System application regarding communications with WEB browsers. -> 3008 Como Gestor de Chão de Fábrica, eu pretendo conhecer o estado atual das máquinas de cada linha de produção.

# 3. Subtasks assignment


## 1040321:
- **4002** - Como SCM, pretendo proceder à recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção.
- **1011** - Como Gestor de Projeto, eu pretendo que a equipa desenvolva uma aplicação que simule o funcionamento de uma máquina, nomeadamente no envio de mensagens geradas por estas.
- **3008** - Como Gestor de Chão de Fábrica, eu pretendo conhecer o estado atual das máquinas de cada linha de produção.

## 1080510:
- **1012** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.
- **6001** - Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.

