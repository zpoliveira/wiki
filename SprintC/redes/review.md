RCOMP 2019-2020 Project - Sprint C planning -> RCOMP spint 4
=========================================
### Sprint master: 1080510 ###

# 1. Sprint's backlog #

## 1040321:
- **4002** - Como SCM, pretendo proceder à recolha das mensagens geradas nas/pelas máquinas de uma determinada linha de produção.
- **1011** - Como Gestor de Projeto, eu pretendo que a equipa desenvolva uma aplicação que simule o funcionamento de uma máquina, nomeadamente no envio de mensagens geradas por estas.
- **3008** - Como Gestor de Chão de Fábrica, eu pretendo conhecer o estado atual das máquinas de cada linha de produção.

## 1080510:
- **1012** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de monitorização do seu estado.
- **6001** - Como Sistema de Monitorização das Máquinas (SMM), pretendo monitorizar o estado das máquinas por linha de produção.

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses	the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * 1 - Totally implemented with no issues
  * 2 - Totally implemented with issues
  * 3 - Partially implemented with no issues
  * 4 - Partially implemented with issues
  * 5 - Not Implemented

- **4002** - 1
- **1011** - 1
- **3008** - 5 no time to execute...
- **1012** - 1
- **6001** - 1


