# Projeto Integrador da LEI-ISEP Sem4 2019-20 SprintC

## User Stories

### Geral

- [US1010 - XSD](../SprintC/userStories/CriarXSD.md)

### Produção

- [US2007 - Exportar Dados XML](../SprintC/userStories/ExportarXMLChaoDeMaquina.md)
- [US2010 - Introduzir Ordem Produção](../SprintC/userStories/OrdemProducaoManual.md)
- [US2011 - Consultar Ordens Produção por Estado](../SprintC/userStories/ConsultarEstadoOrdemProducao.md)
- [US2012 - Consultar Ordens Produção Data Encomenda](../SprintC/userStories/ConsultarOrdemProducaoDeEncomenda.md)

### Chão de Fábrica

- [US3004 - Associar Ficheiro A Maquina](../SprintC/userStories/AssociarFicheiroAMaquina.md)
- [US3005 - Consultar Notificações de Erro](../SprintC/userStories/ConsultarNotificacoesErrosPorTratar.md)
- [US3006 - Arquivar Notificações de Erro](../SprintC/userStories/ArquivarNotificacaoErro.md)
- [US3007 - Consultar Notificações de Erro Arquivadas](../SprintC/userStories/ConsultarNotificacoesErroArquivadas.md)

### Comunicação

- [US1011 - Simulador Maquinas](../SprintC/userStories/SimuladorMaquina.md)
- [US1012 - Emitir Estado Maquina](../SprintC/userStories/EstadoMaquina.md)
- [US3008 - WebDashBoard](../SprintC/userStories/MonitorizarEstadoMaquinaPorLinhaWeb.md)
- [US4001 - Importar Mensagens Para Processamento](../SprintC/userStories/ImportarMensagens.md)
- [US4002 - SCM RecolhaMensagens](../SprintC/userStories/RecolherMensagensDeMaquina.md)
- [US6001 - SMM Monitorizar Estado Maquinas](../SprintC/userStories/MonitorizacaoMaquinaPorLinha.md)

### Processamento de Mensagens

- [US5001- Processamento Mensagens](../SprintC/userStories/ProcessarMensagens.md)
