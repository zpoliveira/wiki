# User Story 2007 - Exportar XML

=================================================

## 1. Requisitos

Como Gestor de Produção, eu pretendo exportar, para um ficheiro XML, toda a infomação subjacente ao chão de fábrica (e.g. produtos, matérias-primas, máquinas, linhas de produção, categorias, ordens de produção, fichas de produção, lotes, consumos reais e efetivos, estornos, desvios, tempos de produção, entre outros).

## 2. Análise

### Regras de Negócio

-   Deve ser possivel aplicar filtros temporais sobre os dados a exportar(e.g. ordens de produção, consumos, desvios).
-   O sistema deve ainda perguntar que informação e (tipos de) resultados (e.g. desvios, tempos) devem ser considerados.
-   A exportação deve ser validada pelo XSD elaborado pela equipa (cf. US 1010).

### Testes unitários

-

## 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   Produto
-   CategoriaMateriaPrima
-   MateriaPrima
-   LinhaProducao
-   Maquina
-   Deposito
-   (falta objetos obtidos apos procssamento)

**Controlador:**

-   ExportarFabricaController

**Builder:**

-   FabricaXmlBuilder

### 3.1. Realização da Funcionalidade

-   O Gestor de Produção inicia o processo de exportaçao
-   O sistema pede a localização do ficheiro
-   O Gestor de Produção indica o caminho
-   O sistema localza e valida o caminho
-   O sistema importa todas os objetos existentes no sistema, e exporta para xml

### 3.2. SD / Diagrama de Classes

**SD**
![]()

**Diagrama de Classes**
![]()

### 3.3. Padrões Aplicados

-   Padrão Builder
-   Padrão Strategy

### 3.4. Testes

**null**:

## 4. Implementação

## 5. Integração/Demonstração

## 6. Observações
