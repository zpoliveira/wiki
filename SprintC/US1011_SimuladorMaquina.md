# User Story 1011 - Simular Máquinas

=================================================

# 1. Requisitos

Para efeitos de execução e teste de negócio quer-se poder simular o envio de mensagensd pelas máquinas de modo a validar a qualidade do sistema.

# 2. Análise

## Regras de Negócio

-   As máquinas devem simular execução fisica usando esperas pre determinadas;
-   As máquinas so devem enviar nova mensagem depois de receber sinal de sucesso no envio anterior;


## Testes unitários

-   n/a;

# 3. Design

-  Utilizar estrutura de codigo atribuindo responsabilidades corretas a diferentes partes da implementação; Utilização de ficheiros header para configuração e distribuição de funcionalidades em diferentes sources.

**Classes do domínio:**

- Sistema externo ao dominio

**Controlador:**



## 3.1. Realização da Funcionalidade

- Inicia-se o simulador com a especificão do ficheiro a ler, o tempo em segundos de cadência e o id da identificação da maquina.
- O simulador envia inicialmente uma mensagem Hello e posteriromente cada linha do ficheiro (mensagem) com o tempo de cadência especificado.
- A cada mensagem enviada recebe uma resposta a qual é persistida na memória partilhada. 

Fluxos alternativos:
    - No caso de perda de ligação após um periodo determinado de time-out a máquina gera uma mensagem NACK para memória partilhada.
    - Quando o último estado da máquina é um NACK esta pode receber um pedido para envio de nova mensagem Hello para o sistema de comunicação máquinas.

### 3.1.1 Atribuição de responsabilidades

|                                                      | Responsável | Razão |
| :--------------------------------------------------- | :---------: | ----: |
| O sistema inicia a simulação                         |  main.c           |   inicio do programa    |
| Instancia memoria partilhada                        |    gestor_memoria.c         | separação de responsabilidade      |
| Instancia processos por numero de linhas de produção |             |       |
| Inicia threads para a execuçao de cada máquina       |             |       |
|                                                      |             |       |
|                                                      |             |       |

## 3.2. SD / Diagrama de Classes

**SD**


**Diagrama de Classes**
![CD_SimularMaquina.png](CD_SimularMaquina.png)

## 3.3. Padrões Aplicados

- Padrão State 

## 3.4. Testes

**testFicheiroNull:** Verificar que não é possível importar um ficheiro inexistente ou nulo.

**testFicheiroEmpty:** Verificar que não é possível importar um ficheiro sem conteudo, ou com conteudo imcompleto.

# 4. Implementação

-  A implementaçã será realizada em C usando threads, deste modo as nomenclaturas OOP serão utilizadas no sentido de separar responsabilidades, e melhorar a estrutura do software final.

# 5. Integração/Demonstração

# 6. Observações
