# Welcome

Welcome to our wiki! This wiki is intended to clarify key aspects / components of LAPR4 project..

## Project Description

## Links to important Documents

- [SprintA DomainModel](./SprintA/DomainModel.md)
- [SprintB DomainModel](./SprintB/DomainModel.md)
- [SprintC DomainModel](./SprintC/DomainModel.md)
- [SprintD DomainModel](./SprintD/DomainModel.md)

## Glossary

- [Glossary](./Glossary.md)

## User Stories

+[SprintB](./SprintB/UserStoriesB.md)

+[SprintC](./SprintC/UserStoriesC.md)

+[SprintD](./SprintD/UserStoriesD.md)

+[SprintE](./SprintE/UserStoriesE.md)

### Planning

- [FunctionsPerStudent](./Planning/StudentsFunctions.md)

- [Planning](./Planning/Planeamento.md)

## Important Decisions

+[SprintB](./SprintB/DecisionsB.md)

+[SprintC](./SprintC/DecisionsC.md)

+[SprintD](./SprintD/DecisionsD.md)

+[SprintE](./SprintE/DecisionsE.md)

### Archive

#### Sprint_A

- [Sprint1 doubts](./SprintA/archival/Sprint1_Questoes.md)
- [Respostas EAPLI](./SprintA/archival/RespostasEAPLI.md)
- [Respostas Fórum LAPR4](./SprintA/archival/RespostasLAPR.md)
