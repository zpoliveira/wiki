# Projeto Integrador da LEI-ISEP Sem4 2019-20

# 1. Constituição do Grupo de Trabalho

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.                          | Nome do Aluno       |
| ---------------------------------- | ------------------- |
| **[1040321](./Student1040321.md)** | José Pedro Barbosa  |
| **[1181846](./Student1181846.md)** | Miguel João Pereira |
| **[1161335](./Student1161335.md)** | José Pedro Oliveira |
| **[1080510](./Student1080510.md)** | Nuno Miguel Silva   |

# 2. Distribuição de Funcionalidades

A distribuição de requisitos/funcionalidades ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

## Sprint B

|                      Aluno Nr. | Sprint B                                                                        |
| -----------------------------: | :------------------------------------------------------------------------------ |
| [1040321](./Student1040321.md) | [US1007 - BootMaquina](../SprintB/userStories/BootMaquina.md)                   |
|                                | [US2003 - ConsProdutoSemFP](../SprintB/userStories/ConsProdutoSemFP.md)         |
|                                | [US2004 - EspecificarFP](../SprintB/userStories/EspecificarFP.md)               |
|                                | [US2006 - AddProduto](../SprintB/userStories/AddProduto.md)                     |
|                                |                                                                                 |
| [1181846](./Student1181846.md) | [US1008 - BootLinhaProd](../SprintB/userStories/BootLinhaProd.md)               |
|                                | [US2005 - ImportProdutosCSV](../SprintB/userStories/ImportProdutosCSV.md)       |
|                                | [US1006 - BootProdutos](../SprintB/userStories/BootProdutos.md)                 |
|                                |                                                                                 |
| [1161335](./Student1161335.md) | [US1004 - BootMatPrima](../SprintB/userStories/BootMatPrima.md)                 |
|                                | [US2001 - AddMatPrima](../SprintB/userStories/AddMatPrima.md)                   |
|                                | [US2002 - DefinCatMatPrima](../SprintB/userStories/DefinCatMatPrima.md)         |
|                                | [US1005 - BootCatMatPrima](../SprintB/userStories/BootCatMatPrima.md)           |
|                                |                                                                                 |
| [1080510](./Student1080510.md) | [US1009 - BootDeposito](../SprintB/userStories/BootDeposito.md)                 |
|                                | [US3001 - DefinMaquina](../SprintB/userStories/DefinMaquina.md)                 |
|                                | [US3002 - EspecificarLinhaProd](../SprintB/userStories/EspecificarLinhaProd.md) |
|                                | [US3003 - EspecificarDeposit](../SprintB/userStories/EspecificarDeposit.md)     |

## Sprint C

|                      Aluno Nr. | Sprint C                                                                                                            |
| -----------------------------: | :------------------------------------------------------------------------------------------------------------------ |
| [1040321](./Student1040321.md) | [US4002 - SCM RecolhaMensagens](../SprintC/userStories/RecolherMensagensDeMaquina.md)                               |
|                                | [US5001 - Processamento Mensagens](../SprintC/userStories/ProcessarMensagens.md)                                    |
|                                | [US3008 - WebDashBoard](../SprintC/userStories/MonitorizarEstadoMaquinaPorLinhaWeb.md)                              |
|                                | [US3004 - Associar Ficheiro A Maquina](../SprintC/userStories/AssociarFicheiroAMaquina.md)                          |
|                                | [US4001 - Importar Mensagens Para Processamento](../SprintC/userStories/ImportarMensagens.md)                       |
|                                |                                                                                                                     |
| [1181846](./Student1181846.md) | [US2007 - Exportar Dados XML](../SprintC/userStories/ExportarXMLChaoDeMaquina.md)                                   |
|                                | [US2009 - Importar Ordens Produção](../SprintC/userStories/ImportarOrdemProducaoCSV.md)                             |
|                                | [US1011 - Simulador Maquinas](../SprintC/userStories/SimuladorMaquina.md)                                           |
|                                | [US4001 - Importar Mensagens Para Processamento](../SprintC/userStories/ImportarMensagens.md)                       |
|                                |                                                                                                                     |
| [1161335](./Student1161335.md) | [US2010 - Introduzir Ordem Produção](../SprintC/userStories/OrdemProducaoManual.md)                                 |
|                                | [US2011 - Consultar Ordens Produção por Estado](../SprintC/userStories/ConsultarEstadoOrdemProducao.md)             |
|                                | [US3006 - Arquivar Notificações de Erro](../SprintC/userStories/ArquivarNotificacaoErro.md)                         |
|                                | [US3007 - Consultar Notificações de Erro Arquivadas](../SprintC/userStories/ConsultarNotificacoesErroArquivadas.md) |
|                                | [US2012 - Consultar Ordens Produção Data Encomenda](../SprintC/userStories/ConsultarOrdemProducaoDeEncomenda.md)    |
|                                |                                                                                                                     |
| [1080510](./Student1080510.md) | [US1012 - Emitir Estado Maquina](../SprintC/userStories/ConsultarEstadoMaquinaPorLinha.md)                          |
|                                | [US6001 - SMM Monitorizar Estado Maquinas](../SprintC/userStories/MonitorizacaoMaquinaPorLinha.md)                  |
|                                | [US1010 - XSD](../SprintC/userStories/CriarXSD.md)                                                                  |
|                                | [US3005 - Consultar Notificações de Erro](../SprintC/userStories/ConsultarNotificacoesErrosPorTratar.md)            |
|                                |                                                                                                                     |

## Sprint D

|                      Aluno Nr. | Sprint D                                                                                                          |
| -----------------------------: | :---------------------------------------------------------------------------------------------------------------- |
| [1040321](./Student1040321.md) | [US5002 - Processamento Recorrente de Mensagens](../SprintD/userStories/ProcessamentoRecorrenteMensagens.md)      |
|                                | [US1013 - Proteção Comunicações SCM Maquinas](../SprintD/userStories/ProtegerComunicaçõesEntreSCMeMaquinas.md)    |
|                                | [US1015 - Proteção Comnunicações Maquinas SCM](../SprintD/userStories/ProtegerComunicaçõesEntreSimuladorESCM.md)  |
|                                | [US9002 - Apresentação](../SprintD/userStories/Apresentacao.md)                                                   |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
| [1181846](./Student1181846.md) | [US2013 - Aplicar Transformação XML](../SprintD/userStories/AplicarTransformaçãoXML.md)                           |
|                                | [US1017 - Relatorio XML_XSD_XSLT_XPATH](../SprintD/userStories/RelatorioXML_XSD_XSLT_XPATH.md)                    |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
| [1161335](./Student1161335.md) | [US3009 - Estado Processamento/Ultima execução](../SprintD/userStories/AlterarEstadoSPMPorLinha.md)               |
|                                | [US3010 - Solicitar Envio ficheiro Maquina](../SprintD/userStories/SolicitarEnvioConfiguracaoParaMaquina.md)      |
|                                | [US9002 - Apresentação](../SprintD/userStories/Apresentacao.md)                                                   |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
| [1080510](./Student1080510.md) | [US3011 - Solicitar reinicialização](../SprintD/userStories/SolicitarReinicializacaoDeMaquina.md)                 |
|                                | [US6002 - SMM enviar pedido Reinicializaçãp](../SprintD/userStories/SMMEnviarPedidoReinicializacaoParaMaquina.md) |
|                                | [US1017 - Relatorio XML_XSD_XSLT_XPATH](../SprintD/userStories/RelatorioXML_XSD_XSLT_XPATH.md)                    |
|                                |                                                                                                                   |
|                                |                                                                                                                   |
| [1050469](./Student1050469.md) | [US1014 - Simulador recebe ficheiros](../SprintD/userStories/SuporteSimuladorMaquinaParaFicheirosConfiguracao.md) |
|                                | [US1016 - Simulador pedidos reinicialização](../SprintD/userStories/SimuladorAceitarReset.md)                     |
|                                |                                                                                                                   |

### RCOMP

#### Sprint 4

- [Planning](../SprintC/redes/planning.md)
- [Review](../SprintC/redes/review.md)

#### Sprint 5

- [Planning](../SprintD/redes/planning.md)
- [Review](../SprintD/redes/review.md)
