# Sprint A - LEI 2º Ano 2º Sem 2019 2020

## Objetivo de funcionalidade

Como principal objectivo pretende-se recolher uma diversidade de mensagens obtida das maquinas para realizar o processamento desses dados para que o sistema forneça informações para avaliar controlo da execução das ordens de produção. Neste sentido, permitirá verificar se determinada ordem se encontra ou não em produção, calcular tempos (bruto e efetivo de produção), consumo de matérias primas (e desvios relativamente a um padrão) e quantidade de produtos produzidos (bem como o lote a que pertencem). Ou seja perceber como decorreu a produção.

Pretende-se ainda disponibilizar infromação relevante a sistemas externos e integrar informação util existente nos mesmos no sistema a desenvolver.

Notas: 

* De acordo com Vaughn Vernon em "Effective Aggregate Design - Part II: Making aggregates work together" (pag. 8) , *"Prefer references to eexternal aggregates only by their globally unique indentity, not by holding a direct object reference."*. por essa razão, optamos por fazer as relações entre agregados utilizando o seu identificador (por exemplo, CodigoInternoMaquina), em vez de com a Entidade (de acordo com o anterior exemplo, Máquina).

* Os conceitos de Desvio, Tempo Efetivo e Tempo Bruto, entre outros, não foram evidenciados no Modelo de Domínio uma vez que os seus valores são calculáveis a partir de outros cuja representação já existe no mesmo (Consumos/produções e Data/Hora de Mensagem de início/fim de produção).

## Divisões de Modelo de Domínio

Para facilitar a construção do Modelo de Domínio, optamos por dividi-lo nas diferentes áreas funcionais descritas no caderno de encargos. Isto permitiu-nos dividir o problema global em problemas menores, simplificando a sua compreensão e representação. Salientamos que estes são passos intermédios que nos levaram ao Modelo de domínio final, pelo que devem ser compreendidos como rascunhos demonstrativos dos raciocínios parciais.  

* **

* **Área de gestão de produção**

* **

  * *Breve Descrição*
    * Definição/inserção da informação no sistema assim como a consulta das diversas informações relativas às ordens de produção obtidas através do processamento das mensagens;

* **

  * *Casos de Uso*

  * 4.1.1 - Definição de catálogo de matérias-primas. Uma matéria-prima caracteriza-se essencialmente por um código interno, uma descrição, uma categoria e uma ficha técnica (documento pdf). 

  * 4.1.2 - Definição de categorias de matérias-primas: por exemplo, plásticos, cortiças, vernizes. 

  * 4.1.3 - Definição do catálogo de produtos. Um produto caracteriza-se por um código único de fabrico, um código comercial, uma descrição breve e outra completa.

      * a) Esta informação pode ser criada manualmente por um utilizador e/ou importada de um ficheiro de texto gerado por um sistema externo;

      * b) Complementarmente, deve ser criada e associada a cada produto a informação relativa à sua ficha de produção. A inexistência de uma ficha de produção inviabiliza a produção do produto.

  * 4.1.4 - Criar ordens de produção. Uma ordem de produção caracteriza-se por ter um identificador, uma data de emissão, uma data prevista de execução, a identificação do produto a produzir e das encomendas (apenas identificadores) que fundamentam a respetiva ordem. Por omissão, uma ordem de produção assume o estado de “pendente”.

      * a) Esta informação pode ser criada manualmente por um utilizador e/ou importada de um ficheiro de texto gerado por um sistema externo;

      * b) Uma ordem de produção posse assumir outros estados:

          * i) “Em Execução” ou “Execução Parada Temporariamente” ou “Concluída”. Estes estados são atribuídos automaticamente com base em informação de mensagens enviadas pelas máquinas;

          * ii) “Suspensa”. Atribuído manualmente por um utilizador.

      * c) Com base nas mensagens enviadas pelas máquinas devem ainda ser atualizados e/ou calculados as informações relativas aos consumos e respetivos desvios, tempos de atividade e lotes produzidos;

  * 4.1.5 - Consultar informação diversa sobre/relacionada com ordens de produção. Alguns exemplos são:

      * a) Para um dado intervalo de tempo, listar todas as ordens de produção em que tenha ocorrido desvios nos consumos de matérias-primas relativamente ao previsto na ficha de produção dos respetivos produtos. Para cada uma das ordens de produção apresentada deve ser possível verificar detalhadamente os consumos reais e respetivos desvios.

      * b) Para um dado produto e intervalo de tempo, listar as ordens de produção realizadas e respetivos tempos de execução (brutos e efetivos), bem como o tempo médio (bruto e efetivo) de execução por unidades produzidas.             

      * c) Para um dado intervalo de tempo, listar todos os consumos de matérias-primas ocorridos e respetivos totais agregados por depósito e por matéria-prima. Deve ser possível aplicar filtros (e.g. matéria-prima, depósito).  

* **

  * *Diagrama:*
  
    ![Diagrama de MD](MD4.1.jpg)

* **

* **Área de gestão de chão de fábrica**

* **

  * *Breve Descrição*
    * Definição e inserção dos elementos pertencentes chão de fábrica assim como a solicitação do processametno de mensagens e gestão de notificações;

* **

  * *Casos de Uso*

  * 4.2.1 - Especificação de máquinas: uma máquina possui um código interno, um número de série, uma descrição, uma data de instalação, uma marca e modelo. 

      * a) Outros atributos correlacionados com o protocolo de comunicação podem ser necessários, em particular, a existência de um outro identificador especificado pelo protocolo. 

      * b) Deve existir a possibilidade de associar à máquina um ou mais ficheiros de configuração complementados com uma breve descrição. A edição/manutenção do conteúdo destes ficheiros é realizada fora do âmbito do sistema a desenvolver. 

  * 4.2.2 - Criação de uma linha de produção: por simplificação assume-se que uma linha de produção é apenas uma sequência continua de máquinas.  

  * 4.2.3 - Definição de depósitos: um depósito de matérias-primas e produtos caracteriza-se por um código alfanumérico e uma descrição.

  * 4.2.4 - Ativar/Desativar o processamento de mensagens: 

      * a) Ativar consiste em informar o sistema que o processamento de mensagens deve ocorrer de forma recorrente para uma determinada linha de produção. Em particular, é necessário especificar o intervalo de tempo (e.g. 15m) e o momento a partir do qual se inicia a contagem.

      * b) Desativar consiste em informar o sistema que se pretende suspender o processamento recorrente de mensagens para um determinada linha de produção. 

  * 4.2.5 - Solicitar o processamento de mensagens: quando o processamento recorrente de mensagens está suspenso, o sistema deve permitir que seja executado o processamento de mensagens para uma determinada linha de produção e para um intervalo de tempo especificado (e.g. das 11h45 às 12h00).

  * 4.2.6 - Listar notificações de erros de processamento: a lista deve distinguir entre (i) notificações ativas ou não tratadas e (ii) notificações arquivadas, i.e. que já foram tratadas por alguém. 

      * a) A mudança de estado (ativa/arquivada) de uma notificação deve ser realizada explicitamente por um utilizador do sistema; 

      * b) Adicionalmente, as notificações devem estar classificadas em/por tipos de erro.
 

* **

  * Diagrama:

    ![Diagrama de MD](4.2.png)
    
* **

* **Área de comunicação com as máquinas**

* **

  * *Breve Descrição*
    * Pré-valida mensagens (retira mensagens duplicadas, com formatos desconhecidos e datas futuras);
    * Envia mensagens de configuração de máquinas;
    * Verifica estado de cada máquina;

  * **

  * *Casos de Uso*

   * 4.3.1 - Recolher mensagens geradas pelas máquinas: as mensagens geradas pelas máquinas podem estar em diferentes formatos e estruturas.

       * a) Primeiramente, este processo deverá ser feito por importação de ficheiros de texto contendo um conjunto de mensagens de uma ou mais máquinas; 

         * **A forma de importação pertence à camada de aplicação**

      * b) Posteriormente, este processo será realizado através da implementação de um protocolo aplicacional em que cada máquina envia as mensagens geradas (uma a uma) para o sistema; 

        * **Neste momento, não aplicável**

      * c) Pré-validação das mensagens: existe necessidade de pré-validar as mensagens relativamente a: 

        *    Mensagem com formatos/estruturas desconhecidas; 
        *    Mensagens com datas futuras (diferenças de dias e não de segundos);
        *    Mensagens duplicadas (i.e. com os mesmos atributos base); 
        *    Todas estas mensagens devem ser rejeitadas. Contudo, deverá existir um log/registo das mesmas (e.g. em ficheiro de texto);

          * **Pré-validação foi considerada um *Serviço*, que incide sobre a *Entidade* Mensagem**

      * d) Após recolha das mensagens, estas devem ficar disponíveis no sistema para posterior processamento.

   * 4.3.2 - Enviar configuração para máquina: o sistema envia para uma dada máquina as mensagens constantes no ficheiro de configuração indicado pelo utilizador, correspondente à configuração que se pretende que a máquina assuma.

   * 4.3.3 - Monitorizar o estado atual do chão de fábrica: visa explorar o protocolo de comunicação com as máquinas para apresentar um visão do estado atual de cada uma das máquinas.  

      * **Este é um caso tipico de uma acção de "reporting", listagens, consultas, e não de lógica de negócio. Normalmente estes casos de uso não tem implicações no modelo de dominio e apenas são usados nesta fase para "validação". Ou seja, da leitura deste casos de uso não devem surgir novos conceitos de dominio uma vez que este caso de uso apenas apresenta informação existente nos conceitos de dominio manipulados noutros casos de uso.**
  
  * **

  * *Diagrama:*
  
    ![Diagrama de MD](4.3.png)


  * **

* **Área de processamento de mensagens**

 * **

  * *Breve Descrição*
    * Processamento de blocos de mensagens com a validação e enriquecimento dos dados;

 * **

  * *Casos de Uso*

  * 4.4.1 - Processar bloco de mensagens: de forma a atualizar e gerar nova informação no sistema, é necessário processar as mensagens recolhidas e já disponíveis no sistema. Este processamento deve ser realizado de forma independente e no limite em paralelo por linha de produção de modo a que a existência de erros que impeçam continuar o processamento numa determinada linha de produção não tenham impacto no processamento de mensagens relativas a outra linha de produção. Entre outros aspetos, este processo deve: 

       * a) Validar as mensagens: cada mensagem deve ser validada. Em particular, destaca-se a necessidade de: 

          * i) Tipo de dados inválidos (alfanumérico em campo inteiro);

          * ii) Referência a elementos não especificados no sistema (e.g. ordens de produção não carregadas no sistema; matéria-prima não definida). 

      * b) Enriquecer as mensagens: as mensagens validadas podem ter necessidade de serem complementadas com informação adicional. Considerar apenas a necessidade de:

          * i) Complementar a mensagem com a linha de produção em que a máquina se encontra;

          * ii) Complementar a mensagem com informação relativa à ordem de produção correspondente; 

          * iii) Alguns casos requerem conhecimento da existência de outras mensagens; 

      * c) Gerar/Atualizar informação com base nas mensagens. A geração/atualização de informação com base nas mensagens deve ser feita:

          * i) Para mensagens datadas num intervalo de tempo específico (e.g. 11h45 às 12h00); ou 

          * ii) Em intervalos de tempo (e.g. 10 minutos) a contar de um dado momento (e.g. 10h00).  

      * d) Erros ocorridos durante este processo devem gerar notificações que informem adequadamente os utilizadores sobre o erro ocorrido e, dessa forma, facilite a realização de operações de retificação.

* **

  * *Diagrama:*

    ![Diagrama de MD](4.4.png)

* **

* Área de administração ***(não será desenvolvida neste trabalho)***

* **

# Modelo Dominio
 ![Modelo Dominio](mainMD.png)

* **

# Modelo Dominio com Agregados
 ![Modelo Agregados](mainMD_aggregates.png)
