# Sprint 1 EAPLI

## Respostas de EAPLI

* **

* *Relativamente ao desperdício que é referido pelo cliente, podemos considerar que esse desperdício retorna para o depósito e atua como matéria prima? Ou seja, se na Ordem de Produção A eu produzir 300 canecas de plástico e tiver um desperdício de 20 kg de plástico, este vai para o depósito e atua como matéria-prima. Se após a Ordem A terminar, se iniciar uma Ordem de Produção B de produção de tampas de plástico, essa ordem pode utilizar o desperdício da anterior como matéria prima? Se este for o caso, no relatório da Ordem de Produção A é considerado que houve 20kg de desperdício ou que houve 0 desperdício visto que este foi reutilizado?* 

  * **tudo que máquina produz tem um código associado. no teu exemplo a máquina gerou 300 canecas (código P1) e 20 kg de "restos de plástico" (código P2). esse é o facto que interessa para os relatórios e cálculos de desvios. se a fábrica utilizou esses 20 kg de restos de plásticos posteriormente ou os deitou ao lixo não é tratado neste sistema. há industrias onde esses "restos" serão incorporados noutros produtos ou serão vendidos a outras fábricas e há outras industrias onde serão deitados ao lixo (verdadeiros desperdícios)** 

* **

* *Relativamente a uma questão colocada ao cliente sobre os produtos derivados de um produto, a resposta foi a seguinte: "(...) 2. No caso de produtos derivados a ficha de produção é a mesma. Note-se que os produtos derivados (e.g. RC1.1.; RC1.2) resultam de uma variação em aspetos qualitativos de um mesmo produto base (e.g. RC1.0). Essa variação resulta do próprio processo produtivo e dificilmente é controlável. Como tal, ao dar-se ordem de produção de um desses produtos não há garantias que o resultado seja esse produto e/ou um dos outros."
Por isso, coloco a seguinte questão: devemos considerar uma geração espontânea de produtos? Ou estes vão estar previamente definidos no catálogo de produtos?*

  * **não existe geração espontanea. todos os outputs das máquinas são conhecidos no sistema no seu catalogo de produtos/materiais**  

* **

* *Em relação ao enunciado do projeto entregador, surgiu uma duvida em relação aos depósitos e gostariamos de ter uma visão da prespetiva de engenharia de aplicações. Ao longo do enunciado é referido depósito de saída e de entrada, depósitos esses que fazem ou entrar matéria-prima na linha de produção ou fazem sair produtos finais dessa mesma linha. 
Porém, no ponto 2.3, no tipo de mensagem "Estorno" é referido que é possível sair de uma máquina uma determinada quantidade de matéria-prima cujo destino é um depósito e que pode sair tanto como matéria futuramente reutilizavél ou como estorno. 
Posto isto, como devemos apresentar (e se é suposto) que qualquer máquina possa enviar matéria-prima de volta para o depósito de entrada? Também é nos dito que o objetivo é "apenas de gerar movimentos de stock" e não "fazer controlo de stocks" isso de certa forma apaga o conceito de depósito de saida?*

  * **no "nosso" sistema não há controlo de stocks nem dos depósitos, pelo que com base nas mensagens recebidas das máquinas iremos registar os consumos e as produções.
  exemplo de "mensagens brutas" de uma mesma ordem de produção
  M1 retirou 3 unidades de P1 do depósito D1
  M2 retirou 2 unidades de P1 do depósito D1
  M1 retirou 4 unidades de P2 do depósito D2
  M1 retirou 2 unidades de P1 do depósito D1
  M2 estornou 1 unidade de P1 para o depósito D1
  M2 produziu 2 unidades de P3 para o deposito D2
  neste caso houve os seguintes consumos:
  6 unidades de P1 do depósito D1
  4 unidades de P2 do depósito D2
  e as seguintes produções:
  2 unidades de P3 para o deposito D2
  o facto do depósito ser de entrada ou saida tem apenas a ver com o facto de ter sido retirado ou produzido/estorno algo e não é uma carateristica do depósito**

* *Mas no enunciado é claramente dito: "M1: responsável pelo transporte de matéria-prima (e.g. placas de cortiça) de um depósito de entrada (DE1)" e 
"chegou ao depósito de saída “DS3” ". Ou seja temos depósitos de entrada que têm uma nomenclatura 'DE' e de saída 'DS'. 
O professor está a dizer que o tipo de depósito não é um value object porque simplesmente não existe esse conceito ?*

  * **no caderno de encargos, esses são exemplos. uma dada fábrica pode querer ter a sua nomenclatura dessa forma porque até tem um deposito fisicamente junto à primeira máquina da linha e outro depósito junto à ultima máquina da linha. Se for mais facil para vocês podem assumir este cenário na descrição geral do vosso modelo de domínio e glossário**

* **

* *As datas devem ser representadas como valueObjects ou como atributos no Modelo de Domínio?*

  * **Uma data é por definição um value object, infelizmente as classes Java para datas eram mutáveis (Date e Calendar) mas isso mudou com o Java 8 (LocalTime, Instant) que passaram ser imutáveis e verdadeiros value objects.
  O termo “atributo” não deve ser confundido com o tipo do atributo uma classe Encomenda tem um atributo ”data de entrega “ do tipo Data (value object). Assim como pode ter um outro atributo “notas” do tipo String e um atributo “produto” que é uma referência para uma entidade do tipo Produto. Atributos são características de uma classe/conceito.
  Se a vossa questão era se “data de entrega”, “data de xxx”, “data de yyy” deveriam ser tipos diferentes ou se são atributos diferentes mas do mesmo tipo (sendo q o tipo é que é o conceito) , então terão que pensar se há regras de negócio diferentes para esses “termos”. Se sim, serão tipos diferentes (diferentes classes. Se não serão apenas “nomes de atributos” mas o seu tipo! A classe value object) será só uma. No caso das datas poderão utilizar as classes standard de datas do Java 8 em vez de criarem as vossas classes.**  

* **

* *em relação ao caso de uso 4.1.5:Consultar informação diversa sobre/relacionada com ordens de produção. Alguns exemplos são: 
a. Para um dado intervalo de tempo, listar todas as ordens de produção em que tenha ocorrido desvios nos consumos de matérias-primas relativamente ao previsto na ficha de produção dos respetivos produtos. Para cada uma das ordens de produção apresentada deve ser possível verificar detalhadamente os consumos reais e respetivos desvios. 
b. Para um dado produto e intervalo de tempo, listar as ordens de produção realizadas e respetivos tempos de execução (brutos e efetivos), bem como o tempo médio (bruto e efetivo) de execução por unidades produzidas. 
c. Para um dado intervalo de tempo, listar todos os consumos de matérias-primas ocorridos e respetivos totais agregados por depósito e por matéria-prima. Deve ser possível aplicar filtros (e.g. matéria-prima, depósito).*
 
  * **Este é um caso tipico de uma acção de "reporting", listagens, consultas, e não de lógica de negócio. 
  Normalmente estes casos de uso não tem implicações no modelo de dominio e apenas são usados nesta fase para "validação". Ou seja, da leitura deste casos de uso não devem surgir novos conceitos de dominio uma vez que este caso de uso apenas apresenta informação existente nos conceitos de dominio manipulados noutros casos de uso. 
  A leitura deste caso de uso poderá ser util apenas para validarem se de facto possuem (identificaram anteriormente) todos os conceitos que são necessários para poder listar essa informação. 
  Questões como totais agregados tipicamente são calculados pela ferramenta de reporting. 
  Em termos de implementação o que se aconselha é:
   existir um repositório apenas para as pesquisas de reporting (vamos ver um exemplo nas TP no projecto eCafetria) separado de todos os outros agregados
   esse repositório retornar DTOs (vamos falar na TP) e não objectos de dominio 
   na camada de aplicação ter um serviço que funcionaria como a ferramenta de reporting e que faz os agrupamentos e totais necessários para devolver à UI 
  por exemplo, para a alinea c) o vosso Repositório deveria devolver a seguinte estrutura (código de material, designacao de material, código de deposito, designacao de deposito, nr de unidades consumidas, data de consumo) sendo depois calculado no na camada de aplicação os totais por deposito e por matéria prima**  

* **

* **não se esqueçam do que falamos sobre o modelo de dominio nas aulas Teoricas e estarem a capturar conceitos e associações que na realidade não interessam ou não existem no modelo de dominio do sistema em questão.Por exemplo, "Máquina gera mensagem" é algo que não faz sentido ser colocado neste modelo de dominio. Neste sistema as mensagens já foram geradas pelas máquinas, como tal a existir no modelo de dominio seria algo do género "Mensagem foi gerada numa máquina". Ou seja, a associação não é de Máquina para mensagem mas sim de Máquina para Mensagem. O modelo de dominio é sempre elaborado na perspectiva do sistema que estamos a construir e não na perspectiva "agnóstica" ou generalista**


* **Outro aspeto importante, relacionado com a aula teorica de hoje, no modelo de dominio existirão pouquissimos, ou nenhum, "verbos". Devem focar a vossa análise nos "nomes" existentes no discurso do cliente, no caderno de encargos e outros documentos. Operações (verbos) apenas nas situações como falamos na aula em que se vá aplicar o conceito de Domain Service: quando essa operação não é naturalmente atribuida a um objecto concreto**

* **


* **mais algumas notas que vos podem ajudar:
pensem na ficha de produção como sendo o que é necessário para produzir um produto, ou seja, quais são os "constituintes" de um produto. Por exemplo:
um parafuso para ser produzido necessita de uma barra de ferro (que depois será torneada e cortada)
uma cadeira necessita ("é constituída") duas tábuas, quatro barras de ferro e 10 parafusos
os parafusos usados como constituinte da cadeira são os mesmo parafusos do primeiro exemplo. neste caso a fábrica vende parafusos e cadeiras. as barras de ferro e as tábuas são materiais/matérias primas (os termos são usados quase como sinónimos no caderno de encargos) 
aconselhamos a dividirem o modelo em áreas de acordo com a organização funcional que o caderno de encargos indica que o cliente organiza o seu sistema, e que coloquem em cada área apenas os conceitos dessa área. Obviamente conceitos de uma área poderão referir-se (ter associações) a conceitos de outra área. Para os conceitos dentro de uma área cataloguem-os em seguida como sendo «Entity» ou «Value Object» e depois agrpem-os em agregados identificando qual a sua «root entity».
Uma das áreas fundamentais é a área relacionada com o controlo de execução, onde existirão muitos conceitos associados à ordem de produção mas que dizem respeito apenas ao controlo da execução. é aqui que vão dar resposta a pedidos do cliente como, por exemplo, quanto tempo é que a ordem de produção X esteve a ser trabalhada na máquina Y.**

* **


* **No modelo de domínio não devem existir conceitos como “sistema” ou “empresa” a menos que façam realmente parte do domínio e haja casos de uso de manipulação desses conceitos.
Tenham cuidado com os nomes das relações entre conceitos, evitem verbos tipo “tem”, “inserido”, etc. Tentem realmente perceber qual a relação que faz sentido. Não esqueçam de especificar as cardinalidade das relações.
Como falamos nas teóricas, vejam se não estão a modelar coisas demasiado abertas que na realidade o sistema não necessita. Vejam o exemplo das moradas e da relação bidirecional  com conta e cliente. Os casos de uso são uma boa indicação da necessidade de certos conceitos e das relações e direção dessas relações.
Por outro lado vejam se não estão a simplificar e esquecer conceitos. Um exemplo concreto neste enunciado é a ficha de produção. Trata-se de um conceito complexo relacionado com alguns “subconceitos” que devem estar presentes no modelo.
Em princípio, no modelo de domínio também não aparecem conceitos correspondentes a ações/operações (verbos) mas apenas nomes/substantivos. Há exceções mas para já podem assumir que não existem conceitos tipo “processamento” ou “validação”.
Por último, em relação ao objectivo principal deste sistema a desenvolver:
Controlo da execução de ordens de produção. Ou seja, saber como decorreu a produção .**

