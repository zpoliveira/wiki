# Sprint 1 EAPLI

## Objetivo de funcionalidade

Como principal objectivo pretende-se recolher uma diversidade de mensagens obtida das maquinas para realizar o processamento desses dados para que o sistema forneça informações para avaliar o estado de execução das ordens de produção. Neste sentido, permitirá verificar se determinada ordem se encontra ou não em produção, calcular tempos (bruto e efetivo de produção), consumo de matérias primas (e desvios relativamente a um padrão) e quantidade de produtos produzidos (bem como o lote a que pertencem).

Pretende-se ainda disponibilizar infromação relevante a sistemas externos e integrar informação util existente nos mesmos no sistema a desenvolver.


## Outros objetivos

* Enviar ficheiros de configuração para máquinas;
* Monitorizar o estado actual de todas as máquinas;

## Perguntas

* O sistema pode ter alguma influência na linha de produção?
  * 4.1 3 b.: "A inexistência de uma ficha de produção inviabiliza a produção do produto."
  * Pag 3: "Um dos objetivos principais deste sistema consiste **em automatizar o controlo de execução** das ordens de produção";

* No caso de o processamento das mensagens estar ativo de forma recorrente e caso existam erros a unica ação sera a notificação?

* Como o cliente pretende obter a informação para automatizar o controlo da execução das ordens de produção, através da exploração da informação advinda das mensagens recebidas, isto também será um objectivo?

## Divisões de Modelo de Domínio

* Área de gestão de produção
  * Breve Descrição
    * Definição/inserção da informação no sistema assim como a consulta das diversas informações relativas às ordens de produção obtidas através do processamento das mensagens;
  * Diagrama:
  
    ![Diagrama de MD](4.1v2.png)

* Área de gestão de chão de fábrica
  * Breve Descrição
    * Definição e inserção dos elementos pertencentes chão de fábrica assim como a solicitação do processametno de mensagens e gestão de notificações;
  * Diagrama:

    ![Diagrama de MD](4.2.png)

* Área de comunicação com as máquinas
  * Breve Descrição
    * Pré-valida mensagens (retira mensagens duplicadas, com formatos desconhecidos e datas futuras);
    * Envia mensagens de configuração de máquinas;
    * Verifica estado de cada máquina;

  * Diagrama:
  
    ![Diagrama de MD](4.3.jpg)

* Área de processamento de mensagens
  * Breve Descrição
    * Processamento de blocos de mensagens com a validação e enriquecimento dos dados;
  * Diagrama:

    ![Diagrama de MD](4.4.jpg)

* Área de administração ***(não será desenvolvida neste trabalho)***

## Duvidas do Modelo de Dominio

* A representação do sistema está correcta? (Representa-se o conceito Sistema ou vários conceitos, por exemplo: Pré-Validação, Validação, Enriqueciento e Processamento);

* Faz sentido os conceitos processamento ou enrequecimento, etc... (uma vez que nos parecem processos) aparecerem no Modelo de Domínio?
