# Sprint 1 EAPLI

## Respostas no Fórum de LAPR

* **

*  *É relevante para o sistema identificar tipos de desperdício? Possuem identificação própria para além de XP1 (i.e. XP2, XP3, etc)?* 

  * **Os "desperdicios" gerados têm uma identificação própria e correspondem a matérias-primas. Ou seja, o "desperdicio" obtido na produção do produto A pode ser usado como matéria-prima na produção de outro produto (e.g. B). No limite, o "desperdicio" de facto é zero porque será reutilizado noutra coisa. "**

* **

*  *Na página 5 (secção 2.2) é-nos dito que: 
Em resultado disto, e após processamento das respetivas mensagens, o sistema deve associar a cada ordem de produção os movimentos de stock (entrada/saída) de produtos e matérias-primas ocorridos nos depósitos, o período durante a qual a mesma decorreu, as máquinas utilizadas e durante quanto tempo cada uma esteve em atividade e/ou parada, os produtos e quantidades obtidas bem como os respetivos lotes. 
As quantidades que interessam para o nosso sistema são as quantidades que chegam aos depósitos ou também as quantidade produzidas por cada máquina? O cálculo de desvios deve ser calculado com base em que informação?* 

  * **Interessam todas as quantidades: as que são produzidas, as referentes a estornos e as consumidas pelas máquinas. Em face disto, entram e saem quantidades dos depósitos. 
  No mesmo depósito podem ocorrer entradas e saídas de matérias-primas.
  Desvios: com base na ficha de produção conforme diz no paragrafo seguinte do texto que citas. "Note-se ainda que as quantidades de matérias-primas consumidas podem ser distintas daquelas que, de acordo com a ficha de produção do produto em causa, seriam previsivelmente consumidas. Estas diferenças (i.e. desvios) devem ser identificadas e quantificadas."**

* **

*  *Na página 9 do enunciado, ponto 4.4, é dito "Processar bloco de mensagens". Isto quer dizer que apenas podemos processar as mensagens bloco a bloco? Sendo estes blocos aqueles mencionados na página 2, ponto 2.1a): "mensagens produzidas pelas máquinas são disponibilizadas ao sistema a desenvolver em blocos"* 

  * **No enunciado o uso da palavra "bloco" refere-se a um conjunto de mensagens. Na secção 2.1.a) (pág. 2), foi usada no sentido de sugerir que cada ficheiro tem um conjunto de mensagens. Note-se que, no limite, pode ser um ficheiro com as mensagem da Máquina 1 e outro com mensagens da Máquina 2, etc... 
  Na secção 4.4.1 (pág. 9), foi usada no sentido de processar um conjunto de mensagens. Que conjunto de mensagens? Os obtidos pelas hipóteses indicadas nos pontos imediatamente abaixo: 4.4.1.c).i) e 4.4.1.c).ii).Note-se que como cliente, não vejo problemas em que as mensagens, após a sua recolha, sejam imediatamente processadas pelo sistema. 
  Contudo, devem ter em consideração as consequências (positivas e negativas) dessas decisões no funcionamento geral do sistema.**

* **

*  *Deverá existir um conceito de stock para além dos conceitos movimento de stock e matéria-prima uma vez que está fora do âmbito do projeto de EAPLI?* 

  * **PAM - Lamento, mas não me parece que isto seja uma dúvida para ser respondida pelo cliente.
  PAG - Não sei se entendi a tua pergunta totalmente. Neste sistema não são geridos stocks, pelo que não é necessário saber que existem na fábrica 100 unidades do produto X. essa responsabilidade é de outro sistema. Este sistema apenas necessita saber os consumos e as produções para depois informar o outro sistema que fará a sua lógica de negócio para determinar qual o stock de cada produto/matéria**

* **

*  *O que acontece depois de ser gerada uma mensagem? Acontece qualquer ato de persistência/ de arquivação (com o raw message ou com a mensagem já validada e enriquecida)? Porque pode ou não haver um registo de informação que dê origem ás ações tomadas. Ou a longevidade da mensagem só é entre a comunicação de entidades?* 

  * **Não tenho a certeza de ter compreendido a tua questão. Quem gera as mensagens são as máquinas. As máquinas, quando existe comunicação, transmitem as mensagens por si geradas para o sistema a desenvolver. A partir daí, o sistema faz o que precisa de fazer com a mensagem (validar, enriquecer, persistir, eliminar, processar, etc...). Essa decisão já é de cada grupo de trabalho.**

* **

*  *O calculo do desvio é referente apenas ao produto alvo da ordem de produção ou também relativo a todas as fichas de produção dos produtos efetivamente produzidos?* 

  * **A situação mais habitual é a ordem de produção solicitar a produção do produto A e o produto produzido ser também A. 
  Noutros casos, a ordem de produção solicita a produção do produto A (e.g. rolha) e são obtidos outros produtos (e.g. B e C) derivados de A. Sendo que B e C representam qualidades distintas de A. Esta informação deveria ser suportada pelo sistema e influenciar o calculo dos desvios.
  No exemplo do enunciado, se a ordem de produção fosse de 600 unidades de RC1.0 então o desvio deveria ser ZERO pois foram produzidos 310 de RC1.1, 205 de RC1.1 e 85 de RC1.3.
  Note-se que a obtenção de outros produtos não derivados de A serão indicados como estorno de matéria-prima. 
  Em qualquer dos casos, diferenças entre a quantidade indicada na ordem de produção e as efetivamente produzidas podem sempre existir. Isto não implica nenhuma notificação. Será reportado através de listagens para o efeito.**

* **

*  *Atendendo ao exemplo no enunciado:
"Vi M2 indica que produziu 400 unidades de “RC1.0” (i.e. rolhas de cortiça) referentes ao lote “L1”;
Viii M3 indica que obteve 200 unidade de “RC1.1” (i.e. Rolhas Premium);"
Tendo em vista que a mensagem 8 não corresponde à uma produção, mas sim à uma classificação de um produto cuja produção já foi informada anteriormente (mensagem 6) e cujo lote já fora especificado, devemos concluir a existência de um tipo de mensagem de "classificação de produto"?* 

  * **No exemplo (secção 2.2) as mensagens vi) e viii) são do tipo "produção" (cf. secção 2.3). Contudo, podes assumir que as mensagens relativas aos consumos de M3 não estão explicitamente mencionadas no exemplo. M3 está a consumir a produção de M2.**

* **

*  *No exemplo dado qual é o produto associado á ordem de produção "OP0030”? É o RC1.0 ou algum dos outros identificados (RC1.1 e RC12 e RC1.3)? A ficha de produção para RC1.0 e por exemplo RC1.1 é a mesma? Por tanto o produto e os seus derivados têm a mesma ficha de produção?* 

  * **No exemplo dado a ordem de produção poderia ser relativa a qualquer um dos produtos: RC1.0, RC1.1, RC1.2 ou RC1.3. Dependeria do produto encomendado pelo cliente.
  No caso de produtos derivados a ficha de produção é a mesma.
  Note-se que os produtos derivados (e.g. RC1.1.; RC1.2) resultam de uma variação em aspetos qualitativos de um mesmo produto base (e.g. RC1.0).Essa variação resulta do próprio processo produtivo e dificilmente é controlável. Como tal, ao dar-se ordem de produção de um desses produtos não há garantias que o resultado seja esse produto e/ou um dos outros.**

* **

*  *No enunciado é referido que, através das mensagens enviadas pelas máquinas, o sistema deve ir calculando os consumos das várias matérias-primas que foram necessários para realizar a ordem. Esses valores serão depois comparados com os valores "teóricos" que constam na ficha de produção do produto, ou seja, serão calculados os desvios entre o valor "teórico" e o valor que realmente foi consumido. Após explorar este conceito surgiu-nos uma questão em relação ao cálculo desses desvios:
Os desvios devem ser calculados sempre que um utilizador necessita de os consultar, ou aquando calculados junto com os consumos, ficam registados nalguma parte do sistema? Porque se os desvios forem recalculados sempre que um utilizador pede, se houver alterações à ficha de produção do produto, o desvio será sempre em relação à ficha de produção atual, ao passo que se ficar registado, será em relação aos valores que a ficha de produção indicava na altura de realização da Ordem.
Além disso, surgiu-nos outra questão. Durante o cálculo do consumo de uma matéria-prima, o valor vai sendo incrementado. Isto porque, por exemplo, o sistema processa uma mensagem que reporta o consumo de 50 rolhas, e mais tarde uma outra mensagem reporta o consumo de mais 30. A questão que aqui se coloca então é se o valor do desvio em relação ao valor de produção (por exemplo 90), deve ir sendo recalculado por cada mensagem, ou seja, primeiro toma o valor 40 e depois 10, ou só deve ser calculado uma única vez, depois de ter sido calculado o consumo total de rolhas. Colocamos esta questão pois se um utilizador puder pedir ao sistema para ver os consumos e desvios enquanto o sistema ainda está a processar as mensagens da ordem, os valores de desvios ainda não estariam disponíveis no segundo caso.* 

  * **Esta questão menciona várias coisas às quais vou tentar responder:
  Cálculo dos Desvios: se os desvios são calculados sempre que necessário (e.g. para apresentar numa listagem) ou se os cálculos são efetuados e os seus resultados persistidos parece-me claramente uma questão técnica e, portanto, como cliente não me pronuncio.
  Alterações à Ficha de Produção: de facto ao longo do tempo podem existir alterações à ficha de produção de um produto. Contudo, estas a ocorrer serão pontuais e, por tal motivo não foi visto como algo prioritário para o sistema a desenvolver. Com efeito, se quiseres podes "preparar" o teu sistema para essa posterior evolução.
  Ordem de Produção e sua informação: nota que as ordem de produção têm um estado que se pretende que seja gerido automaticamente. Nas listagens e outras funcionalidades faz sentido utilizar esse estado para informar o utilizador da "validade" dos resultados apresentados.
  Para complementar a resposta ao 1º ponto. nesta fase podem colocar no modelo de domínio que o "desvio" é um "valor derivado" e posteriormente decidirem que o tornam persistente ou não. A questão vai colocar-se para mais alguns atributos e terão que analisar com que frequência é que esses dados serão requisitados e se o utilizador pode ou não ficar à espera que o valor seja calculado. vai haver vantagens e desvantagens. o cenário é semelhante ao cálculo do saldo da conta bancária que falamos na aula teórica de EAPLI**

* **

*  *Poderia-me esclarecer se o ficheiro de configuração que se encontra ligado às maquinas contém mensagens ou se as máquinas é que detém as mensagens e que depois se encontram ligadas a um/vários ficheiro de configuração?* 

  * **Para o sistema a desenvolver, o conteúdo dos ficheiros de configuração é pouco ou nada relevante. Esse conteúdo é texto que poderá ter que ser enviado pelo sistema para a máquina usando o protocolo de comunicação existente. As mensagens mencionada 4.3.2 não têm qualquer relação com as mensagens que o sistema deverá recolher, validar e processar.**

* **

*  *O código comercial de cada produto é único, ou diferentes produtos podem ter o mesmo código?* 

  * **O código comercial também é único.**

* **

*  *Na página 5 do enunciado, onde são especificados os tipos de mensagens, é sempre utilizado o termo "matéria prima". Por exemplo, no caso das mensagens de consumo: "A matéria-prima e a respetiva quantidade constam sempre na mensagem". (Questão 1) No entanto, esta "matéria prima" pode ser tanto uma matéria prima como um produto correto? (Questão 2) Se sim, temos garantia de que o código dos produtos serão sempre diferentes dos códigos das matérias primas?* 

  * **A ambiguidade do termo "matéria-prima" já foi esclarecida em resposta a uma pergunta colocada aqui no fórum. Relembro que antes de colocarem aqui perguntas, devem verificar se as mesmas não foram anteriormente respondidas.**

* **

*  *Durante a análise do enunciado, surgiram-nos dúvidas em relação ao armazenamento das matérias primas nos depósitos. Por um lado, assumimos por simplificação, que cada depósito só pode armazenar uma matéria-prima. No entanto, gostaríamos de confirmar esta suposição. Por outro lado, tínhamos também assumido que só existiria um depósito para cada matéria-prima. Ou seja, para além de um depósito X só poder armazenar parafusos, esse depósito X seria o único depósito de parafusos. No entanto, na página 8, secção 4.1, alínea 5. c. é referido a possibilidade de "listar todos os consumos de matérias-primas ocorridos e respetivos totais agregados por depósito e por matéria-prima". Ora, com esta nossa segunda suposição, listar os consumos por depósito ou por matéria-prima teria o mesmo resultado, pelo que nos perguntamos: Poderá haver diversos depósitos a armazenar o mesmo produto? (Ex.: Depósitos X e Y armazenam parafusos, Depósitos A, B e C armazenam pregos)* 

  * **Num depósito podem estar armazenados várias matérias-primas. E a mesma matéria-prima pode estar armazenada em vários depósitos.**

* **

*  *Como forma de melhor nos integrarmos no contexto do sistema, gostaríamos de entender melhor em que momento é feito o processamento das mensagens provenientes de máquinas. As ordens de execução inseridas no nosso sistema ainda estão por executar, e é enquanto ocorrem que é feito o processamento das mensagens enviadas pelas máquinas? Ou as ordens de execução inseridas já ocorreram, e o nosso sistema interpreta as mensagens que resultaram da ordem, como se se tratasse de um "historial"? Ou podem ocorrer casos das duas situações anteriores? É claro que, em qualquer dos casos, ainda seria feito o enriquecimento das ordens de execução através das mensagens. A nossa equipa, após discussão, está a tender para o 3º caso, em que ocorrem ambas as situações, mas gostaríamos de confirmar esta questão.* 

  * **Normalmente, quando as ordens de produção são inseridas no sistema ainda não foram executadas. E, portanto, ainda não haverá mensagens das máquinas relativas a essas ordens de produção. Contudo, pode ocorrer situações (por razões diversas) em que as mensagem recebidas mencionem ordens de produção desconhecidas do sistema. Neste caso, essas mensagens devem gerar uma notificação de forma a que as ordens de produção desconhecidas sejam introduzidas no sistema. Após esta introdução, o sistema deve ser capaz de reprocessar essas mensagens e, desta vez, sem erros.**

* **

* *Uma outra dúvida que nos surgiu foi a seguinte: Sabe-se que uma ordem é referente a um e um só produto e a uma quantidade (por exemplo uma ordem para produzir 50 rolhas). No entanto, através do enunciado, e mais especificamente através da secção de exemplos de mensagens, vê-se que é possível uma ordem gerar diversos produtos diferentes. Por exemplo, diferentes qualidades de rolha são consideradas produtos distintos com códigos de produtos distintos, e mesmo a cortiça residual pode ser considerada um produto. Assim, questionamo-nos se poderá surgir uma situação em que foram pedidas 50 unidades do produto: "rolhas qualidade A" mas em que, após execução da ordem, apenas chegaram ao depósito 30 rolhas qualidade A, e as restantes unidades serem outros produtos. Uma vez que o nosso sistema compara e gere informações como os produtos produzidos nas ordens, deveria ser enviada alguma notificação neste caso?* 

*  *Uma outra questão relacionada: Se houver uma ordem de produção para 50 unidades de produto "rolha", esse produto "rolha" será distinto dos produtos que surgem no final da execução da ordem (que seriam por exemplo "rolha A" e "rolha B"), pelo que chegarão aos depósitos 0 unidades "rolha". Nesse caso teríamos de considerar que "rolha A" e "rolha B" eram subtipos de "rolha" e somar indiscriminadamente? É que tal remeteria para uma hierarquia de produtos que já não parece fazer parte do nosso sistema. No entanto, sem esta, nunca serão produzidos produtos "rolha".* 

  * **A situação mais habitual é a ordem de produção solicitar a produção do produto A e o produto produzido ser também A. Noutros casos, a ordem de produção solicita a produção do produto A (e.g. rolha) e são obtidos outros produtos (e.g. B e C) derivados de A. Sendo que B e C representam qualidades distintas de A. Esta informação deveria ser suportada pelo sistema. Note-se que a obtenção de outros produtos não derivados de A serão indicados como estorno de matéria-prima. Em qualquer dos casos, diferenças entre a quantidade indicada na ordem de produção e as efetivamente produzidas podem sempre existir. Isto não implica nenhuma notificação. Será reportado através de listagens para o efeito.**

* **

* *Em relação às informações com que o sistema deve enriquecer as mensagens, gostaríamos de confirmar algumas suposições por nós feitas, uma vez que se trata de uma parte importante do nosso sistema.No caso da Ordem de Produção, o enunciado refere que nem todas as máquinas poderão conseguir identificar a ordem, e que nesse caso teriam de deduzir a partir de mensagens de outras máquinas. Podemos assumir então, por simplificação, que a primeira máquina consegue sempre identificar a ordem? Por outro lado, no caso da Linha de Produção, o enunciado refere que essa informação estará sempre no sistema. Supusemos então que nenhuma mensagem gerada pela máquina conterá inicialmente a informação sobre a linha de produção, e que será sempre adicionada durante o processamento desta.* 

  * **As duas assunções são válidas.**

* **

* *Após análise do enunciado, entendemos que poderão existir diversos Sistema Externos e que estes poderão atuar como utilizadores. Ou seja, em vez de um funcionário com função de Gestor de Chão de Fábrica introduzir manualmente informações sobre as máquinas, essa informação seria fornecida pelo Sistema Externo através de ficheiros. Assim sendo, dá a entender que Sistema Externos diferentes poderão desempenhar funções diferentes (Gestor de Chão de Fábrica, Gestor de Produção).A nossa questão aqui seria:Os Sistema Externos podem fornecer todas as informações que funcionários humanos forneçam? Ou haverá certas informações que têm de ser dadas ou por uns ou por outros?* 

  * **Os sistemas externos providenciam basicamente informação sobre produtos e ordens de produção.**

* **

* *A dúvida era apenas se o enriquecimento de mensagens é esperado que seja feito nos casos em que falta alguma mensagem de alguma máquina (por exemplo, se a ultima maquina já disse que terminou de produzir então a anterior já terminou também) ou se falta algum tipo de informação/dados na mensagem. Estamos confusos em relação a qual destes, ou ambos, o enriquecimento se refere.* 

  * **O enriquecimento diz respeito apenas à informação/dados da mensagem.**

* **

* *No enunciado, página 3, parágrafo 2, é dito que que os sistemas externos fornecem informação de ordens e encomendas ("Esta informação (encomendas e ordens de produção) é controlada/gerida nos sistemas externos"). Como é que esta informação é transmitida ao sistema?* 

  * **Por importação de ficheiros gerados pelo sistema externo.**

* **

* *No enunciado, página 8, é dito "Ativar consiste em informar o sistema que o processamento de mensagens deve ocorrer de forma recorrente para uma determinada linha de produção.". Poderia esclarecer o significado de "recorrente" neste contexto?* 

  * **"recorrente", in Dicionário Priberam da Língua Portuguesa [em linha], 2008-2020, https://dicionario.priberam.org/recorrente [consultado em 25-03-2020].2. Que volta a aparecer, a acontecer, a ser feito.**

* **

* *Nas páginas 8-9 do enunciado (ponto 4.2.4) é dito que o processamento de mensagens é ativado e desativado para linhas de produção específicas, mas não identifica a linha de produção como parâmetro de agendamento. No entanto, é necessário saber a linha de produção se o agendamento é feito por linha de produção. Poderia clarificar esta situação?* 

  * **Compreendeste bem. O agendamento (4.2.4a) é por linha de produção.**

* **

* *Existem regras/restrições para os seguintes valores?*
*1. Número de série das máquinas*
*2. Marca de uma máquina*
*3. Modelo de uma máquina*
*4. Descrição de uma máquina*
*5. Código alfanumérico dos depósitos*
*6. Password do utilizador*
*7. Descrição breve de um produto*
*8. Descrição completa de um produto* 

  * **Para alguns caso, existe:- Não existem máquinas com o mesmo número de série;- Os códigos dos depósitos não excedem os 10 caracteres;- Password: os critérios variam de cliente para cliente e assentam sempre na número de caracteres, uso de minúsculas e maisculas e números;- A descrição breve de uma produto tem no máximo 30 caracteres.**

* **

* *Após análise do enunciado, entendemos que uma ordem de execução (referente sempre a um só produto) pode ser justificada por várias encomendas. Ou seja, Se houver duas encomendas a pedir 50 parafusos cada, poderá haver apenas uma única ordem de produção que produza 100 parafusos. No entanto, surgiu-nos a questão inversa. Uma encomenda apenas pode pedir um e um só produto? Ou poderemos ter uma encomenda que refira, por exemplo, 50 parafusos e 10 pregos?* 

  * **Neste domínio as encomendas não existem pelo que essa questão não se coloca. As encomendas fazem parte de outro sistema (outro domínio) e apenas temos neste sistema a referência da encomenda, a sua identidade. Da mesma forma que no Portal do ISEP tem o NIF (identidade de contribuinte) de cada aluno mas não tem a Entidade Contribuinte.**

* **

* *Em certas partes do enunciado é dito que os tempos brutos e efetivos de execução são relativos a máquinas (página 3, 2º parágrafo: "detalhe de tempos (brutos e efetivos) por máquina"), enquanto que em outras partes é dito que são relativos a ordens de execução (página 8, ponto 5b). Qual é o correto ou qual é a relação entre os 2? De maneira a possivelmente clarificar a dúvida, informo que assumi que certas máquinas da mesma linha de produção poderão estar paradas enquanto outras estão a trabalhar devido a terem diferentes velocidades e tarefas.* 

  * **Os tempos brutos e efetivos de execução aplicam-se a ambos: máquinas e ordens de produção. Exemplo: Considere-se a seguinte informação relativa a uma ordem de produção realizada numa linha com apenas duas máquinas:** 
  **Máquina 1:**
  **- Início/Fim Atividade: 10h00 / 10h40;**
  **- Paragens (Início/Fim): 10h10 /10h15;** 
  **Máquina 2:**
  **- Início/Fim Atividade: 10h35 / 11h00;**
  **Resultados:**
  **- Máquina 1:**
  **-- Tempo Bruto: 0h40**
  **-- Tempo Efetivo: 0h35** 
  **- Máquina 2:** 
  **-- Tempo Bruto: 0h25**
  **-- Tempo Efetivo: 0h25**
  **- Ordem de Produção:** 
  **-- Tempo Bruto: 1h00**
  **-- Tempo Efetivo: 0h55**

* **

* *No enunciado, página 8, primeira frase, é dito que os produtos têm ficha de produção, mas que "a inexistência de uma ficha de produção inviabiliza a produção do produto". Poderia confirmar que o que está frase pretende transmitir é que a ficha de produção é opcional? E no caso de ser opcional, como devemos proceder em relação aos cálculos de desvios de consumo de matérias aquando da produção de produtos que não possuam ficha de produção?* 

  * **O que a frase pretende transmitir é isso mesmo que tu identificaste. Sem ficha de produção, não é possível efetuar esses e outros cálculos e, portanto, o sistema deverá gerar uma notificação (cf. ponto 4.4.1.d).**

* **

* *Tenho já uma uma entidade que guarda o registo de todas as matérias primas (Repository). Pensei que esta entidade poderia funcionar como catálogo. No entanto, com este esquema, se certa informação for removida do catálogo, certos registos passam a referenciar coisas que não existem. Exemplo: O aço 1122 foi removido do catálogo. Então, o movimento "saída de 2kg de aço 1122 do depósito 344" passa a ser inválido, pois o sistema já não conhece o aço 1122. Então a minha questão é: é possível a remoção de uma matéria prima do catálogo? Se for, será necessária uma nova entidade para manter a informação coerente na base dados, enquanto que se não for, o modelo fica mais simples e a base de dados menos carregada. Essa resposta também se aplica ao catálogo de produtos?* 

  * **Os repositórios não são entidades. O padrão Repository serve para abstrair o mecanismo de acesso a dados e pode ser pensado como uma lista global onde todos as instâncias de um dado agregado residem. Nesse sentido a classe ProductRepository representa todos os produtos do sistema. Neste caderno de encargos o termo catalogo é utilizado no sentido de "todos os X" (ex., catalogo de produtos = todos os produtos) e por isso não necessitam desse conceito no modelo de domínio.
  No entanto, noutros domínios poderia ser necessário o conceito catálogo, por exemplo, o "catalogo primavera/verão 2020" ou o "catalogo outono/inverno 20/21" numa empresa de vestuário. nesse domínio, o conceito catalogo seria uma entidade representada no modelo de domínio, existiria um repositório de catálogos da mesma forma que existiria uma entidade Produto e um repositório de produtos**

* **

* *A informação aqui mencionada diz respeito ao ponto 2.3 do enunciado, presente na página 5. É dito no enunciado, que as mensagens do tipo produção poderão conter informação sobre o lote, opcionalmente. A minha dúvida é se o lote deve ser deduzido ou se é possível que sejam produzidos produtos não associados a lotes. Esta resposta também se aplica às mensagens do tipo entrega de produção?* 

  * **Apesar de raro, pode acontecer haver produção sem lotes.**

* **

* *Bom dia, Em produção podem ser obtidos produtos de várias qualidades (rolha premium, rolha A, rolha B, estorno). Estes devem ser enquadrados no fim da produção como itens diferentes ("código de produto" diferente), ou faz tudo parte do mesmo produto (rolhas com o mesmo "código de produto")  que apresenta uma dimensão adicional "qualidade"?* 

  * **Devem ser encarados como itens diferentes.**

* **

* *O que é que acontece quando a sequência das mensagens não é garantida? (exemplo da cortiça, mensagem da maquina 2 estar a trabalhar antes da maquina 1 recolher do deposito)*

  * **O sistema deve reorganizar as mensagens por data/hora de geração.**

* **

* *Numa primeira instância as mensagens produzidas pelas máquinas são disponibilizadas ao sistema, e o sistema devolve à máquina seguinte para dizer que pode "iniciar" trabalho?*

  * **Não!**

* **

* *As máquinas têm funções específicas ou podem realizar qualquer tipo de tarefa? Caso, tenha funções específicas, como sabemos qual é a sua função? Através do código interno?*

  * **Cada máquina executa funções especificas e para as quais foi concebida. De momento, essa informação é pouco relevante para o sistema.**

* **

* *Há algum(ns) critério(s) de escolha em relação a que linha de produção é atribuída uma ordem de produção?*

  * **Não! Isso é uma problemática para outros sistemas existente na empresa que não nos diz respeito. Saberemos (através das mensagens) apenas em que linha essa ordem de produção foi realizada.**

* **

* *Uma linha de produção pode trabalhar em apenas uma ordem de produção, mas uma ordem de produção pode ser trabalhada em mais que uma linha de produção em simultâneo? (Exemplo: dividir as quantidades para que o processo seja mais rápido)*

  * **Uma ordem de produção é realizada totalmente numa única linha de produção. Em cada momento, numa linha de produção apenas está em execução uma ordem de produção. Depois, dessa ordem de produção terminar, essa linha realizará outra ordem.** 

* **

* *Nesta simplificação de projeto, de acordo com o enunciado, é referido que se deve considerar apenas uma fábrica. Deste modo, consideramos que uma unidade industrial apenas contém uma fábrica, é isso?*

  * **A resposta é sim: considera-se que existe apenas uma e só uma fábrica.**

* **

* *Relativamente à relação entre produto e matéria-prima, devemos considerar que um artigo que é simultaneamente produto e matéria-prima, num mesmo depósito, é armazenado como duas entidades diferentes? Ou é armazenado todo junto? Ou seja, se numa fábrica tivermos o aço como produto e matéria-prima, num mesmo depósito existirão X kg de aço-produto e Y kg de aço-matéria-prima, ou existirão apenas Z kg de aço?*

  * **É só um item independentemente de ser usado como matéria (constituinte) de outro produto. Nota: não é objetivo deste sistema fazer controlo de stocks. Apenas de gerar movimentos de stock.**

* **

* *O nosso grupo identificou uma incongruência no enunciado, pois é dito explicitamente de que as máquinas só fazem sentido no contexto de uma linha de produção, no entanto, um dos casos de uso é especificar as máquinas, que dá a entender que estas são independentes da linha de produção. Devemos tratar as máquinas como dependentes ou independentes da linha de produção?*

  * **Uma máquina apenas existe numa linha de produção. maquinaria industrial são equipamentos pesados que (normalmente) não se mudam de sitio após terem sido colocados na fábrica. o importante aqui é descobrirem a relação entre linha de produção e máquina, qual o seu sentido e quais os invariantes de negócio que realmente são importantes para garantir a consistência interna desse agregado.**

    
