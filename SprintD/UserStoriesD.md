# Projeto Integrador da LEI-ISEP Sem4 2019-20 SprintD

## User Stories

### Geral

- [US9002 - elaborar uma apresentação (módulo competências)](../SprintD/userStories/Apresentacao.md)
- [US1017 - relatório que descreva todas as aplicações de XML, XSD, XSLT e XPATH](../SprintD/userStories/RelatorioXML_XSD_XSLT_XPATH.md)

### Produção

- [US2013 - aplicar uma visualização/transformação a um ficheiro XML](../SprintD/userStories/AplicarTransformaçãoXML.md)

### Chão de Fábrica

- [US3009 - saber e alterar o estado do processamento de mensagens de cada linha de produção e conhecer a última vez que o mesmo se realizou](../SprintD/userStories/AlterarEstadoSPMPorLinha.md)

### Comunicação

- [US1013 - Proteger comunicações entre o SCM e as máquinas](../SprintD/userStories/ProtegerComunicaçõesEntreSCMeMaquinas.md)
- [US1014 -  Suporte no simulador de máquina para receber de ficheiros de configuração.](../SprintD/userStories/SuporteSimuladorMaquinaParaFicheirosConfiguracao..md)
- [US1015 -  Proteger comunicações entre o simulador de máquina e o SCM](../SprintD/userStories/ProtegerComunicaçõesEntreSimuladorESCM.md)
- [US1016 - Simulador de máquina aceitar reset](../SprintD/userStories/SimuladorAceitarReset.md)
- [US3010 - solicitar o envio de configuração para uma máquina](../SprintD/userStories/SolicitarEnvioConfiguracaoParaMaquina.md)
- [US3011 - solicitar a reinicialização de uma dada máquina](../SprintD/userStories/SolicitarReinicializacaoDeMaquina.md)
- [US6002 - SMM enviar um pedido de reinicialização para uma dada máquina](../SprintD/userStories/SMMEnviarPedidoReinicializacaoParaMaquina.md)

### Processamento de Mensagens

- [US5002 - Efetuar o processamento recorrente das mensagens](../SprintD/userStories/ProcessamentoRecorrenteMensagens.md)