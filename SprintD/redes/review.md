RCOMP 2019-2020 Project - Sprint D planning -> RCOMP spint 5
=========================================
### Sprint master: 1080510 ###


# 1. Sprint's backlog #

1. Development of the Central System application features regarding communications with Industrial Machines.  
   1.1. **US1013** - Como Gestor de Projeto, eu pretendo que as comunicações entre o SCM e as máquinas estejam protegidas.  
   1.2. **US3010** - Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.

2. Development of the Industrial Machines application regarding communications with the Central System.
    2.1. **US1014** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte a recepção de ficheiros de configuração.  
    2.2. **US1015** - Como Gestor de Projeto, eu pretendo que as comunicações entre o simulador de máquina e o SCM estejam protegidas.  


3. Development of the Industrial Machines application regarding communications with the Monitoring System.  
    3.1. **US1016** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de reinicialização (reset) do seu estado.  

4. Development of the Monitoring System application regarding communications with Industrial Machines.  
    4.1. **US6002** - Como SMM, eu pretendo enviar um pedido de reinicialização para uma dada máquina.

5. Development of the Monitoring System application regarding communications with WEB browsers.  
    5.1. **US3011** - Como Gestor de Chão de Fábrica, eu pretendo solicitar a reinicialização de uma dada máquina.

# 2. Subtasks assessment #
One by one, each team member presents his/her outcomes to the team, the team assesses 		the accomplishment of the subtask backlog.
The subtask backlog accomplishment can be assessed as one of:

  * 1 - Totally implemented with no issues
  * 2 - Totally implemented with issues
  * 3 - Partially implemented with no issues
  * 4 - Partially implemented with issues
  * 5 - Not Implemented

For the last three cases, a text description of what has not been implemented and present issues must be added.
Unimplemented features and issues solving is assigned to the same member on the next sprint.

1. **US1013** - 1
2. **US1015** - 1
3. **US3010** - 1  
4. **US3011** - 1
5. **US6002** - 1
6. **US1016** - 4 
    The Machine reconize the udp Reset request but not do not send the tcp hello to the SCM to reset the machine.

