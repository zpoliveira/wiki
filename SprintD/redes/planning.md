# LAPR4 2019-2020 Project - Sprint D planning -> RCOMP spint 5

### Sprint master: 1080510

(This file is to be created/edited by the sprint master only)

# 1. Sprint's backlog

In this sprint the next use case will be developed:

1. Development of the Central System application features regarding communications with Industrial Machines.  
   1.1. **US1013** - Como Gestor de Projeto, eu pretendo que as comunicações entre o SCM e as máquinas estejam protegidas.  
   1.2. **US3010** - Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.

2. Development of the Industrial Machines application regarding communications with the Central System.
    2.1. **US1014** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte a recepção de ficheiros de configuração.  
    2.2. **US1015** - Como Gestor de Projeto, eu pretendo que as comunicações entre o simulador de máquina e o SCM estejam protegidas.  


3. Development of the Industrial Machines application regarding communications with the Monitoring System.  
    3.1. **US1016** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de reinicialização (reset) do seu estado.  

4. Development of the Monitoring System application regarding communications with Industrial Machines.  
    4.1. **US6002** - Como SMM, eu pretendo enviar um pedido de reinicialização para uma dada máquina.

5. Development of the Monitoring System application regarding communications with WEB browsers.  
    5.1. **US3011** - Como Gestor de Chão de Fábrica, eu pretendo solicitar a reinicialização de uma dada máquina.


# 2. Technical decisions and coordination
    Some of the tasks may need knowledge of other disciplines, so they cannot be completed entirely by one person...

# 3. Subtasks assignment

## 1040321:

1. **US1013** - Como Gestor de Projeto, eu pretendo que as comunicações entre o SCM e as máquinas estejam protegidas. 
2. **US1015** - Como Gestor de Projeto, eu pretendo que as comunicações entre o simulador de máquina e o SCM estejam protegidas.
3. **US3010** - Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.  


## 1080510:

3. **US3011** - Como Gestor de Chão de Fábrica, eu pretendo solicitar a reinicialização de uma dada máquina.
4. **US6002** - Como SMM, eu pretendo enviar um pedido de reinicialização para uma dada máquina.
5. **US1016** - Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte pedidos de reinicialização (reset) do seu estado.
