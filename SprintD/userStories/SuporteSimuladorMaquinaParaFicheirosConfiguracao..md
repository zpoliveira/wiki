# US1014 -  Suporte no simulador de máquina para receber de ficheiros de configuração.

=================================================

# 1. Requisitos

Como Gestor de Projeto, eu pretendo que o simulador de máquina suporte a recepção de ficheiros de configuração.

A interpretação feita deste requisito foi no sentido de que a Simulação de Máquina tem corretamente implementada(e concorrente em linguagem C), o suporte à receção de um ficheiro de configuração de Máquina. A comunicação entre o sistema de comunicação de máquina e o simulador de máquinas não pode ser realizada diretamente, será utilizado o SCM como intermediário. Tendo em conta que já se encontrava implementada a parte de consola (seleção de ficheiro e associação a uma máquina), o foco desta US passará pela ligação e passagem de informação entre o sistema cetral e o SCM e deste com o simulador de máquinas (e vice-versa).


# 2. Análise

## Regras de Negócio

-   Apenas pode selecionar um dos estados de Ordem De produção existentes no sistema de cada vez;
-   Depois de selecionado o estado, só devem ser exibidos os detalhes das Ordens de Produção respetivas;

## Testes unitários

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   OrdemProducao
-	EstadoOrdemProducao

**Controlador:**

-   ConsultarEstadoOrdemProducaoController

**Repository:**

-   OrdemProducaoRepository

## 3.1. Realização da Funcionalidade

-   O Gestor de Produção inicia o processo de consulta de Ordens de Produção por estado
-   O sistema exibe os estados existentes e solicita seleção de uma
-   O Gestor de Produção seleciona estado
-   O sistema exibe Ordens de Produção nesse estado e os seus detalhes

## 3.2. SD / Diagrama de Classes
De forma semelhante a vários dos outros Diagramas de Sequência utilizados, neste também seria criado o Persistence Context, O Repository Factory e depois, a partir deste, os Repositories respetivos. Para facilidade de leitura, estes passos foram omitidos do SD que se segue.

**SD**
![SDConsultarEstadoOrdemProducao](SDConsultarEstadoOrdemProducao.png)

**Diagrama de Classes**
![CDConsultarEstadoOrdemProducao](CDConsultarEstadoOrdemProducao.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Visitor

## 3.4. Testes

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
