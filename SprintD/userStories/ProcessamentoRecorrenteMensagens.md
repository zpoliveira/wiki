# US5002 - Efetuar o processamento recorrente das mensagens
=======================================


# 1. Requisitos

Como Serviço de processamento de mensagens pretendo que sejam enrriquecidas e processadas as mensagens provenientes das maquinas existentes no sistema, de forma recorrente para cada linha de produção. Com os mesmos propósitos/resultados a obter com a user story 5001; 

# 2. Análise

## Regras de Negócio

* O SPM não pode processar mensagens correspondentes a uma linha que não tenha sido selecionada;
* Tem de ser introduzido pelo menos o início do período a analisar assim como a sua recorrencia pelo gestor do chão de fábrica na linha a ser processada (US3009);
* A Ordem de Produção tem de existir;
* O Estado da Ordem de Produção tem de ser atualizada como consequência do SPM;
* A Execução de Ordem de Produção tem de ser atualizada como consequência do SPM;
* Erros ocorridos durante este processo devem gerar notificações.

## Testes unitários  

Testes realizados aos elementos do dominio;
* testeIdentificadorordemProducaoNotNull
* testeDataHoraNotNull
* testeCodigoInternoMaquinaNotNull
* testeIdentificadorordemProducaoNotNull
* testeLinhaProducaoIdentificadorNotNull
* testeCondigoInternoMateriaPrimaNotNull
* testeCodigoAlfanumericoNotNull
* testeQuantidadeNotNull
* testeCodigoFabricoProdutoNotNull
* testeLoteIdNotNull
* testeLoteIdNotEmpty
* testeCodigoParagemNotNull
* testeCodigoParagemNotEmpty



# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

* LinhaProducao
* OrdemProducao
* ExecucaoOrdemProducao (e seus valueObject)
* LoteId
* CodigoParagem
* Mensagem
    * MensagemConsumo
    * MensagemEntregaProducao
    * MensagemEstorno
    * MensagemFimAtividade
    * MensagemInicioAtividade
    * MensagemParagemForcada
    * MensagemProducao
    * MensagemRetomaAtividade
* Notificacao




**Controlador:**

*  EnriquecimentoService
*  ProcessamentoController
*  ProcessamentoRecorrenteService


**Repository:**

* ProdutoRepository
* OrdemProducaoRepository
* MensagemRepository
* MensagemBrutaRepository
* ExecucaoOrdemProducaoRepository
* LinhaProducaoRepository
* NotificacaoRepository


## 3.1. Realização da Funcionalidade

Após a inicialização do serviço o mesmo deve:
* Informar do seu início;
* Verificar todos as linhas que ja tenha configurado como ativo a recorrencia de processamento;
* Nas linhas que o tenham ativo inicia o processamento recorrente;
* Aquando o Gestor de chão de fábrica ativa um processamento recorrente numa linha (US3009) o serviço recebe via socket a linha e adiciona-a aos processamentos recorrentes de acordo com a sua especificação;
* A cada processamento o estado da ordem de produção deve ser atualizado se assim for necessário;
* A cada processamento os dados da Execução da Ordem de produção devem ser atualizados;
* A ocorrencia de erros devem gerar notificações;


## 3.2. SD / Diagrama de Classes

Explicação na implementação

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

Na inicialização do serviço o mesmo verifica no repositorio todas as linhas com recorrencia ativa e utiliza os dados aí definidos para gerar os processamentos recorrentes dessas linhas. O ProcessamentoRecorrenteService gera os Scheduleds para cada linha. No tempo determinado este é executado e através do ProcessamentoService efetua o enriquecimento (EnrequicimentoService) gerando as mensagens com o auxilio da MensagemFactory processando de seguida as mesmas. Esse processamento gera/atualiza a ExecuçãoOrdemProdução onde todos os cálculos (tempos, desvios...) são efetuados em cada um dos seus ObjectValues, uma vez que são os reponsáveis por essa informação.
O estado da ordem de produção também é atualizado neste processo de acordo com o tipo de mensagens na MensagemFactory.
Na ocorrência de um erro é gerada uma notificação com a descrição do erro e qaundo possível informação da mensagem que gerou o erro.


# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações





