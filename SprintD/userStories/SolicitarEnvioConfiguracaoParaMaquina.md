# US3010 - solicitar o envio de configuração para uma máquina

=========================================

# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo solicitar o envio de uma determinada configuração para uma máquina.

A interpretação feita deste requisito foi no sentido de que a Simulação de Máquina tem corretamente implementada(e concorrente em linguagem C), o suporte à receção de um ficheiro de configuração de Máquina. A comunicação entre o sistema de comunicação de máquina e o simulador de máquinas não pode ser realizada diretamente, será utilizado o SCM como intermediário. Tendo em conta que já se encontrava implementada a parte de consola (seleção de ficheiro e associação a uma máquina), o foco desta US passará pela ligação e passagem de informação entre o sistema cetral e o SCM e deste com o simulador de máquinas (e vice-versa).


# 2. Análise

## Regras de Negócio

-   A máquina tem de existir no sistema.
-   A consola tem de utilizar o SCM para comunicar com as máquinas

## Testes unitários

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas.

**Classes do domínio:**

-   Sim


**Controlador:**

-   SolicitarEnvioConfiguracaoParaMaquinaController

**Repository:**

-   MaquinaRepository
-   FicheiroConfiguracaoMaquinaRepository

## 3.1. Realização da Funcionalidade

-   A interação entre o Gestor de Chão de Fábrica e o sistema central, para associação de um ficheiro a uma máquina, já se encontra descrita em        [US3004 - Associar Ficheiro A Maquina](SprintC\userStories\AssociarFicheiroAMaquina.md) 
-   O Gestor de Chão de Fábrica inicia a US
-   O sistema apresenta a lista de máquinas existente
-   O Gestor de Chão de Fábrica seleciona uma máquina
-   O sistema apresenta os ficheiros de configuração que lhe estão associados
-   O Gestor de Chão de Fábrica seleciona um ficheiro
-   O sistema apresenta uma mensagem de sucesso


## 3.2. SD / Diagrama de Classes

**SD**

**Diagrama de Classes**

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Visitor

## 3.4. Testes

# 4. Implementação

-   A interação entre o Gestor de Chão de Fábrica e a consola do sistema cenral já se encontra descrita acima. De acordo com o protocolo de comunicação, seguem-se os seguintes passos, até se completar a US com a entrega do ficheiro de configuração à máquina selecionada:
-   O sistema inicia contacto com o SCM com uma mensagem de Config
-   O SCM responde com uma mensagem de ack
-   O sistema procede ao envio do ficheiro de configuração
-   O SCM envia mensagem de config para a máquina selecionada
-   A máquina responde com ack
-   O SCM envia mensagem com ficheiro de configuração
-   A máquina responde com ack
-   O SCM envia ack para o sistema central
-   Todos estes passos acontecerão via client/server sockets, enviando-se a mensagem utilizando o protocolo TCP

# 5. Integração/Demonstração

Esta US relaciona o sistema central com o SCM e este com o simulador de máquinas. Será criado um client socket no sistema central que comunicará com o server socket já existente no SCM. Por outro lado, a comunicação entre este e o simulador de máquinas acontecerá de forma inversa. 

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
