# US3009 - saber e alterar o estado do processamento de mensagens de cada linha de produção e conhecer a última vez que o mesmo se realizou
=========================================


# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo saber e alterar o estado (ativo/desativo) do processamento de mensagens de cada linha de produção bem como conhecer a última vez que o mesmo se realizou.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fabrica pode verificar para cada linha de produção se o processamento de mensagens se encontra parado ou ativo (recorrente), bem como a data do último processamento.

# 2. Análise

## Regras de Negócio

* A alteração do estado da recorrência de uma linha não pode modificar o estado das restantes.

## Testes unitários
* testMudaEstadoLinha
* testVerificaData
* EstadoProcessamentoNull
* InicioProcessamentoNull
* IntervaloProcessamentoNull
* UltimoProcessamentoNull

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  RecorrenciaProcesssamento
*  LinhaProducao
*  EstadoProcessamento
*  InicioProcessamento
*  IntervaloProcessamento
*  UltimoProcessamento

**Controlador:**

*  AlterarRecorrenciaPorLinhaController

**Repository:**

* RecorrenciaRepository
* LinhaProducaoRepository 


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fabrica inicia o processo de associar verificar/alterar a recorrência das linhas de produção
* O Sistema mostra a lista de linhas de produção, os seus estados de recorrência, último processamento, início da recorrência, intervalo de recorrência e solicita seleção de uma para alteração dos dados
* O Gestor de Chão de Fábrica seleciona a linha pretendida
* O Sistema solicita introdução das modificações
* O Gestor de Chão de Fábrica introduz dados solicitados
* O sistema mostra novos dados da linha selecionada

## 3.2. SD / Diagrama de Classes

## 3.3. Padrões Aplicados

* Padrão Repository


## 3.4. Testes
* testMudaEstadoLinha - Verificar que estado de recorrência é alterado apenas na linha selecionada
* testVerificaData - verificar data do último processamento
* EstadoProcessamentoNull- Nem este campo, nem os seguintes podem estar a null
* InicioProcessamentoNull


# 4. Implementação

* A equipa optou por colocar a definição da recorrência de procemento na aplicação principal, uma vez que o Gestor de Chão de Fábrica tem de lhe ter acesso para efetuar as modificações.
* A recorrencia foi definida como um value object da Linha de Produção, por ser um parâmtro que a define.
* Os dados relativos a cada linha de produção (identificador de linha de produção, estado de processamento, início de processamento, intervalo de processamento e último processamento) serão persistidos no repositório respetivo.


# 5. Integração/Demonstração

* O SPM terá acesso ao repositório indicado acima de modo a que, no momento em que vá proceder ao processamento de mensagens, possa verificar se os parâmetros para a mesma foram modificados.
* O SPM insere na BD o momento do último processamento, sempre que fizer a confirmação anterior e as condições de processamento se mantenham.

# 6. Observações

