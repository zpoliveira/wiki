# US6002 - SMM enviar um pedido de reinicialização para uma dada máquina

## 1. Requisitos

Como SMM, eu pretendo enviar um pedido de reinicialização para uma dada máquina.

## 2. Análise

### Regras de Negócio

- O ID do protocolo tem de existir na lista de máquinas conhecidas.

### Testes unitários

## 3. Design

**Classes do domínio:**

-  Mensagem

**Classes Http Server**

-  HttpServer
-  HttpRequest
-  HttpMessage


**Classes Udp Server**

- UdpSendReset


### 3.1. Realização da Funcionalidade

- O servidor HTTP está a escuta de pedidos
- o servidor HTTP trata o URL recebido para o reset da máquina
- O servidor HTTP solicita a realização do pedido UDP para a máquina em questão

### 3.2. SD / Diagrama de Classes


### 3.3. Padrões Aplicados

-   Padrão Visitor - impressão de mensagem recebidas e envidas na consola

### 3.4. Testes


## 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

## 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

## 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
