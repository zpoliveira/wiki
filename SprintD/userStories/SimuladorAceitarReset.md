# US1016 - Simulador de máquina aceitar reset

=============================================================

# 1. Requisitos

Como simulador de máquina pretendo aceitar sinal de RESET e confirmar a recepcao.

# 2. Análise

## Regras de Negócio

-   ser capaz de receber sinal de reset por UDP;

# 3. Design

-   Divisão de responsabilidades por funcionalidades.
-   Para efeitos da simulação o objeto UdpServer.c é capaz de receber o sinal de RESET, e reponder com sinal HELLO

## 3.1. Realização da Funcionalidade

-   O Simulador recebe sinal RESET
-   O Simulador responde com sinal HELLO

# 4. Implementação

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
