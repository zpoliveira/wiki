# User Story US2013 - aplicar uma visualização/transformação a um ficheiro XML anteriormente gerado

=================================================

# 1. Requisitos

Como Gestor de Chão de Fábrica, eu pretendo poder transformar os dados previamente exportados por mim em xml, para um outro formato.

# 2. Análise

## Regras de Negócio

-   Os formatos são pre-definidos e já existem conversões disponiveis.

## Testes unitários

-   N/A

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas
-   Utilizar estratégia aquando da seleção do formato a exportar, de modo a separar a responsabilidade de definição de ficheiro
-   As classes de estratégia todas generalizão uma classe abstracta que implementa e fornece acesso aos processos de transformação XSLT

**Classes do domínio:**

-   Não tem acesso ao dominio, pois nao necessita desses objetos. Usa apenas os dados previamente exportados.

**Controlador:**

-   TransformXmlDataController

**Strategies:**

-   TransformToHtmlStrategy
-   TransformToTextStrategy
-   TransformToJsonStrategy
-   TransformToXmlStrategy

**Outras:**

-   DataTransformer (Abstracta Generalizada)

## 3.1. Realização da Funcionalidade

-   O Gestor de Chão de Fábrica inicia a transformação de um ficheiro xml
-   O sistema pergunta qual ficheiro a exxportar
-   O sistemas pergunta o formato para o qual transformar os dados
-   O Sistema transforma os dados pretendidos

## 3.3. Padrões Aplicados

-   Padrão Strategy
-   Padrão Controller

# 4. Implementação

-   A equipa entendeu que deveria usar a API Saxon para uma maior facilidade na implementação do processo de transformação XSLT.

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
