# US9002 - elaborar uma apresentação (módulo competências)
=======================================

IMPORTANTE: Este documento não apresenta a descrição de um caso de uso a ser realizado pela a aplicação, como a sua análise e design, mas é sim um documento que serve de guia/resumo para o que se pretende alcançar na e com a apresentação.

# 1. Descrição

Pretende-se elaborar uma apresentação relativa ao modulo de competências, com os seguintes requisitos:
"Como cliente do sistema, eu pretendo que a equipa (fornecedor de software) elabore uma apresentação focando os seguintes aspetos: (i) resultados atingidos; (ii) análise critica dos resultados e do trabalho em equipa adotando o formato SWOT; (iii) evidências da aplicação do processo de engenharia/desenvolvimento de software; (iv) cenário de deployment da solução; (v) qualidade do produto final; (vi) sugestões de melhoria; entre outros que considerem relevantes. 
Pretende-se ainda que cada aluno apresente uma autoavalição relativamente às suas competências técnicas e comportamentais antes e após a realização do projeto integrador. O resultado deve constar de uma tabela/gráfico onde constam todos os elementos do grupo. Sugere-se a adoção de uma escala de avaliação simples (e.g. muito fraco, fraco, razoável, bom, muito bom, excelente)."


# 2. Objetivos 
* Demostrar resultados na optica das competências adquiridas;
* Efetuar uma avaliação critica do trabalho em equipa;
* Evidenciar a aplicação de conceitos e processo de engenharia de software;
* Análisar o percurso de cada ator presente no desenvolvimento da aplicação e efetuar a sua auto-avaliação;
* Apresentar uma reflexão critica ao processo de desenvolvimento do projeto em geral.





