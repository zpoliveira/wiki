# US1017 - relatório que descreva todas as aplicações de XML, XSD, XSLT e XPATH

=================================================

# 1. Requisitos
Como Gestor de Projeto, eu pretendo que a equipa elabore um relatório que descreva todas as aplicações de XML, XSD, XSLT e XPATH no sistema.
O resultado deve ser um documento XML em conformidade com o XSD de relatório disponibilizado, complementado com uma transformação para um documento XHTML que facilite a sua leitura integral.

# 2. Análise

## Regras de Negócio

- A estrutura do xml tinha de responder ao xsd fornecido por LPROG.

## Testes unitários

n/a

# 3. Design

-   Foi estruturado o xml basiado pelas regras fornecidas pelo xml. Não podendo ser 
muito flexivel. Mesmo pelas regras pelo conteúdo XSLT para HTML.


## 3.1. Realização da Funcionalidade

-   Criação do Xml
-   Criação do XSLT
-   Exportação para HTML do XML do relatório

## 3.2. SD / Diagrama de Classes


**Diagrama de Classes**
![relatorioclasse.png](relatorioclasse.png)

## 3.3. Padrões Aplicados


## 3.4. Testes


# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
