# US1013 - Proteger comunicações entre o SCM e as máquinas
=======================================


# 1. Requisitos

Como Gestor de Projeto, eu pretendo que as comunicações entre o SCM e as máquinas estejam protegidas.


# 2. Análise

## Regras de Negócio
Todas as comunicações entre o SCM e as maquinas devem ser efetuadas de forma encriptada/segura através de ssl/tls.

## Testes unitários


# 3. Design



## 3.1. Realização da Funcionalidade



## 3.2. SD / Diagrama de Classes



## 3.3. Padrões Aplicados


## 3.4. Testes 


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*
Geradas as chaves publicas e privadas assim como implementado a conexão segura. 

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



