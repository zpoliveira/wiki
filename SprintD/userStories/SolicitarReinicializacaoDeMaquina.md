# US3011 - solicitar a reinicialização de uma dada máquina
=======================================


# 1. Requisitos

Como Gestor de Chão de Fabrica pretendo solicitar oa recinicilização de uma dada máquina através do webdashboard.

# 2. Análise

Colocação de botão no Webdashboard para solicitar a o reset de uma das máquinas na lista.

## Testes

n/a

# 3. Design

- Divisão de responsabilidades por funcionalidades.


## 3.1. Realização da Funcionalidade

- O utlizador realiza o pedido de RESET
- Através do Java Script é realizado um pedido de ao servidor de HTML, pedido POST com indicando o id da máquina.


## 3.3. Padrões Aplicados



## 3.4. Testes 



# 4. Implementação



# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações





