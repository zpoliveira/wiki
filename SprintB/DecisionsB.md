# Projeto Integrador da LEI-ISEP Sem4 2019-20 SprintB

## Important Decisions

### Estrutura dos Menus

#### Menu Inicial

| Menu             | Opções          |
| ---------------- | --------------- |
| **Menu Inicial** | Bootstrap       |
|                  | Produção        |
|                  | Chão de Fábrica |
|                  | Exit            |

#### Menu Bootstrap

| Menu               | Opções                                 |
| ------------------ | -------------------------------------- |
| **Menu Bootstrap** | Bootstrap Categoria de Matérias Primas |
|                    | Bootstrap Produtos                     |
|                    | Bootstrap Depósitos                    |
|                    | Bootstrap Matéria prima                |
|                    | Bootstrap de Máquina                   |
|                    | Bootstrap de Linha de Produção         |
|                    | Bootstrap total                        |
|                    | Return                                 |

#### Menu Produção

| Menu              | Opções                               |
| ----------------- | ------------------------------------ |
| **Menu Produção** | Definir Nova Categoria Matéria Prima |
|                   | Adicionar Matéria Prima              |
|                   | Adicionar Produto                    |
|                   | Especificar Ficha de Produção        |
|                   | Consulta Produtos sem Ficha Produção |
|                   | Importar Produtos CSV                |
|                   | Return                               |

#### Menu Chão de Fábrica

| Menu                     | Opções                          |
| ------------------------ | ------------------------------- |
| **Menu Chão de Fábrica** | Definir Nova Maquina            |
|                          | Especificar Novo Depósitos      |
|                          | Especificar Nova Linha Produção |
|                          | Return                          |

### Alterações ao MD

- Introdução das modificações sugeridas durante o review do Sprint A (além de outras alterações "menores" foi criado o agregado _Execução de Ordem de Produção_)
- O value Categoria de Matéria-Prima, pertencente à Entity Matéria-Prima, foi passado a Entity, tendo como value o Nome de Categoria.
