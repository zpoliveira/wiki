# User Story 2005 - Importar Produtos CSV

=================================================

# 1. Requisitos

Como Gestor de Produção, eu pretendo importar um ficheiro CSV com uma lista de Produtos para adicionar ao sistema.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção teria a possibilidade de acrescentar uma lista de produtos disponivel em ficheiro CSV,

# 2. Análise

## Regras de Negócio

-   O Produto sem Ficha de Produção não pode existir no sistema;
-   Cada Produto é único;
-   Não sendo possivel importar um dado produto, deve ser registada a falha;

## Testes unitários

-   testFicheiroNull;
-   testFicheiroEmpty;

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   Produto

**Controlador:**

-   ImportarCatalogoController

**Repository:**

-   ProdutoRepository

## 3.1. Realização da Funcionalidade

-   O Gestor de Produção inicia o processo de importação de ficheiro
-   O sistema pede a localização do ficheiro
-   O Gestor de Produção indica o caminho
-   O sistema localza e valida o ficheiro
-   O sistema importa todas os produtos existentes no ficheiro, registando as falhas de importação num ficheiro proprio

## 3.2. SD / Diagrama de Classes

**SD**
![SD_ImportarCatalogo](SD_ImportarCatalogoCSV.png)

**Diagrama de Classes**
![CD_ImportarCatalogo.png](CD_ImportarCatalogoCSV.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Strategy
-   Padrão Iterator

## 3.4. Testes

**testFicheiroNull:** Verificar que não é possível importar um ficheiro inexistente ou nulo.

**testFicheiroEmpty:** Verificar que não é possível importar um ficheiro sem conteudo, ou com conteudo imcompleto.

# 4. Implementação

# 5. Integração/Demonstração

# 6. Observações
