User Story 1004 - Boot Matérias Primas


# 1. Requisitos

Como utilizador da aplicação, eu pretendo iniciar o carregamento de Matérias-Primas para o seu catálogo.

A interpretação feita deste requisito foi no sentido de que qualquer utilizador poderia iniciar o bootstrap de Matérias-Primas, de forma a ser possível testar a aplicação com alguns dados iniciais.  

# 2. Análise

## Regras de Negócio

* As mesmas da definição de matéria-Prima. 

## Testes unitários

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  categoriaMatPrima
*  codigoInternoMateriaPrima
*  descricaomateriaPrima
*  fichaTecnica
*  materiaPrima
*  nomeCategoria

**Controlador:**

*  AdicionarMateriaPrimaController

**Repository:**

*  MateriaPrimaRepository


## 3.1. Realização da Funcionalidade

* O Utilizador inicia o processo de boootstrap de Matérias-Primas
* O sistema apresenta o nome das Matérias-primas carregadas e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

* O diagrama de sequência e o de classes são em tudo semelhantes aos da US1005 - Bootstrap de Categoria de Matéria Prima, representados abaixo. No entanto, em vez de instânciadas Categorias de Matérias-Primas, serão instânciadas Matérias-Primas.

**SD**
![SDBootCatMatPrima](SDBootCatMatPrima.png)

**Diagrama de Classes**
![CDBootCatMatPrima](CDBootCatMatPrima.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

* Para a correta implementação deta US é necessária a existência das fichas técnicas em PDF, com os nomes e nos locais indicados. Nomeadamente, V1.pdf, M1.pdf, A1.pdf, F1.pdf, C1.pdf em c:\.

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*