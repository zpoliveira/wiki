# User Story 1005 - Bootstrap de Categoria de Matéria Prima
=======================================


# 1. Requisitos

Como utilizador da aplicação, eu pretendo iniciar o carregamento de categorias de matéria-prima para o catálogo de categorias.

A interpretação feita deste requisito foi no sentido de que qualquer utilizador poderia iniciar o bootstrap de categorias de matéria-prima, de forma a ser possível testar a aplicação com alguns dados iniciais.  

# 2. Análise

## Regras de Negócio

* As mesmas da definição de categoria de matéria-prima. Nomeadamente:
     * O nomeCategoria da categoria tem existir e ser único

## Testes unitários

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  categoriaMatPrima
*  nomeCategoria

**Controlador:**

*  DefinCatMatPrimaController

**Repository:**

*  DefinCatMatPrimaRepository


## 3.1. Realização da Funcionalidade

* O Utilizador inicia o processo de boootstrap de categoria de matéria-prima
* O sistema apresenta o nome das categorias carregadas e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SDBootCatMatPrima](SDBootCatMatPrima.png)

**Diagrama de Classes**
![CDBootCatMatPrima](CDBootCatMatPrima.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*