# User Story 2001 - Adicionar Matéria-Prima
=======================================


# 1. Requisitos

Como Gestor de Produção, eu pretendo adicionar uma matéria-prima ao catálogo.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção teria a possibilidade de acrescentar uma nova matéria-prima ao conjunto de matérias-primas existentes no sistema. 

# 2. Análise

## Regras de Negócio

* O codigoInternoMateriaPrima tem de ser único no sistema;
* Cada matéria-prima só pode ter uma categoria de matérias-primas;
* Tem de ter descricaoMateriaPrima e fichaTecnica (documento pdf);
* Pode ser considerado produto;

## Testes unitários

* testCodigoInternoNull;
* testCodigoInternoUnico;
* testCategoriaMatPrimaNull;
* testCategoriaMatPrimaNãoExiste;
* testDescricaoMatPrimaNull;
* testFichaTecnicaNull;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  materiaPrima
*  categoriaMatPrima
*  codigoInternoMateriaPrima
*  descricaoMateriaPrima
*  fichaTecnica (documento pdf)

**Controlador:**

*  AddMatPrimaController
*  ListCategoriaMatPrimaService - Para evitar duplicação de código

**Repository:**

*  MatPrimaRepository 
*  CatMatPrimaRepository


## 3.1. Realização da Funcionalidade

* O Gestor de Produção inicia o processo de adição de matéria-prima
* O sistema apresenta uma listagem das categorias de matérias-primas e solicita a escolha de uma
* O Gestor de Produção seleciona uma
* O sistema solicita introdução do código interno de matéria prima
* O Gestor de produção procede com o solicitado
* O sistema solicita introdução da descrição da matéria-prima
* O Gestor de produção procede com o solicitado
* O sistema solicita upload de ficha técnica
* O Gestor de produção procede com o solicitado
* O sistema apresenta os dados introduzidos e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SDaddMatPrima](SDaddMatPrima.png)

**Diagrama de Classes**
![CDaddMatPrima](CDaddMatPrima.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Visitor (Quando lista as categorias de matérias-primas)

## 3.4. Testes 

**testCodigoInternoNull:** Verificar que não é possível a criação de uma matéria-prima sem código interno.

**testCodigoInternoUnico:** Verificar que não é possível a criação de matéria-prima com código interno repetido.

**testCategoriaMatPrimaNull:** Verificar que não é possível a criação de uma matéria-prima sem categoria de matéria-prima.

**testCategoriaMatPrimaExiste:** Verificar que não é possível a criação de matéria-prima com categoria de matéria-prima inexistente.

**testDescricaoMatPrimaNull:** Verificar que não é possível a criação de uma matéria-prima sem descrição.

**testFichaTecnicaNull:** Verificar que não é possível a criação de uma matéria-prima sem ficha técnica.

# 4. Implementação

* Para a correta implementação deta US é necessária a existência de uma ficha técnica em PDF, com nome e em local a indicar pelo utilizador.

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

* Se o Gestor de Produção quiser especificar um produto como matéria-prima tem de, quando essa necessidade surgir, iniciar o caso de uso de especificar matéria-prima.



