# User Story 1007 - Bootstrap de Máquinas
=========================================


# 1. Requisitos

Como utilizador da aplicação, eu pretendo iniciar o carregamento dos máquinas para a aplicação.

A interpretação feita deste requisito foi no sentido de que qualquer utilizador poderia iniciar o bootstrap de máquinas, de forma a ser possível testar a aplicação com alguns dados iniciais.  

# 2. Análise

## Regras de Negócio

* As mesmas da definição de Máquina. Nomeadamente:
    * O codigoInternoMaquina tem de ser único no sistema;
    * O numSerie (nº de série) tem de ser único no sistema;

## Testes unitários

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Maquina
*  DescricaoMaquina
*  CodigoInternoMaquina
*  Marca
*  Modelo
*  NumSerie
*  DataInstalacao

**Controlador:**

*  DefinirNovaMaquinaController

**Repository:**

*  MaquinaRepository 


## 3.1. Realização da Funcionalidade

* O Utilizador inicia o processo de boootstrap de Máquinas
* O sistema apresenta mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

** * O SD e CD são semelhantes aos do Bootstrap de Depósitos abaixo, sendo instânciadas máquinas em vez de depósitos. 

**SD**
![SD_BootDepositos](SD_BootDepositos.png)

**Diagrama de Classes**
![CD_BootDepositos](CD_BootDepositos.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

	@Test(expected = IllegalArgumentException.class)
		public void ensureNullIsNotAllowed() {
		Exemplo instance = new Exemplo(null, null);
	}

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*