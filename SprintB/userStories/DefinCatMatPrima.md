# User Story 2002 - Definir categoria de Matéria Prima
=======================================


# 1. Requisitos

Como Gestor de Produção, eu pretendo adicionar uma categoria de matéria-prima ao catálogo de categorias.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção teria a possibilidade de acrescentar uma nova categoria de matéria-prima ao conjunto de categorias de matérias-primas existentes no sistema. 

# 2. Análise

## Regras de Negócio

* O nomeCategoria da categoria tem existir e ser único

## Testes unitários

* testNomeCategoriaNull;
* testNomeCategoriaUnico;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  categoriaMatPrima

**Controlador:**

*  DefinCatMatPrimaController

**Repository:**

*  DefinCatMatPrimaRepository


## 3.1. Realização da Funcionalidade

* O Gestor de Produção inicia o processo de adição de categoria de matéria-prima
* O sistema solicita introdução do nome da categoria da matéria-prima
* O Gestor de produção procede com o solicitado
* O sistema apresenta o nome da categoria e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SDDefinCatMatPrima](SDDefinCatMatPrima.png)

**Diagrama de Classes**
![CDDefinCatMatPrima](CDDefinCatMatPrima.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory

## 3.4. Testes 

**testNomeCategoriaNull:** Verificar que não é possível a criação de uma categoria de matéria-prima sem nome.

**testNomeCategoriaUnico:** Verificar que não é possível a criação de uma categoria de matéria-prima com nome repetido.

# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



