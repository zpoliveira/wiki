# User Story 1007 - Bootstrap de Máquinas

=========================================

# 1. Requisitos

Como utilizador da aplicação, eu pretendo iniciar o carregamento das linhas de produção para a aplicação.

A interpretação feita deste requisito foi no sentido de que qualquer utilizador poderia iniciar o bootstrap das linhas de produção, de forma a ser possível testar a aplicação com alguns dados iniciais.

# 2. Análise

## Regras de Negócio

-   As mesmas da definição de Linha de Produção. Nomeadamente:
    -   O LinhaProducaoID tem de ser único no sistema;

## Testes unitários

# 3. Design

-   Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

-   Maquina
-   LinhaProducao
-   LinhaProducaoID
-   SequenciaMaquina

**Controlador:**

-   LinhasProducaoBootstrapper

**Repository:**

-   LinhaProducaoRepository

## 3.1. Realização da Funcionalidade

-   O Utilizador inicia o processo de boootstrap de Linhas de Produção
-   O sistema apresenta mensagens de sucesso

## 3.2. SD / Diagrama de Classes

\*\* \* O SD e CD são semelhantes aos do Bootstrap de Depósitos abaixo, sendo instânciadas LinhasProducao em vez de depósitos.

**SD**
![SD_BootDepositos](SD_BootDepositos.png)

**Diagrama de Classes**
![CD_BootDepositos](CD_BootDepositos.png)

## 3.3. Padrões Aplicados

-   Padrão Repository
-   Padrão Factory
-   Padrão Command (no menu de bootstrap)

## 3.4. Testes

**Teste 1:** Verificar que não é possível criar uma instância da classe Exemplo com valores nulos.

    @Test(expected = IllegalArgumentException.class)
    	public void ensureNullIsNotAllowed() {
    	Exemplo instance = new Exemplo(null, null);
    }

# 4. Implementação

_Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;_

_Recomenda-se que organize este conteúdo por subsecções._

# 5. Integração/Demonstração

_Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema._

# 6. Observações

_Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados._
