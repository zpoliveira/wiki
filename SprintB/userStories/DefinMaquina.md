# User Story 3001 - Definir Nova Máquina
=======================================


# 1. Requisitos

Como Gestor de Chão de Fabrica, eu pretendo registar uma nova máquina.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fabrica teria a possibilidade de acrescentar uma nova máquina ao conjunto de máquinas existentes no sistema. 

# 2. Análise

## Regras de Negócio

* O codigoInternoMaquina tem de ser único no sistema;
* O numSerie (nº de série) tem de ser único no sistema;


## Testes unitários

* testCodigoInternoNull;
* testCodigoInternoEmpty;
* testNumSerieNull;
* testNumSerieEmpty;
* testDescNull;
* testDescEmpty;
* testMarcaNull;
* testMarcaEmpty;
* testModeloNull;
* testModeloEmpty;
* testDataInstalacaoNull;
* testDataInstalacaoMaiorQueDiaAtual;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Maquina
*  DescricaoMaquina
*  CodigoInternoMaquina
*  Marca
*  Modelo
*  NumSerie
*  DataInstalacao

**Controlador:**

*  DefinirNovaMaquinaController


**Repository:**

*  MaquinaRepository 


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fabrica inicia o processo de inserção de registo de máquina
* O sistema solicita os dados necessários código interno, descrição, número de série, marca, modelo, data de instalação
* O sistema apresenta os dados introduzidos e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SD_DefMaquina](SD_DefMaquina.png)

**Diagrama de Classes**
![CD_DefMaquina](CD_DefMaquina.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

**testCodigoInternoNull:** Verificar que não é possível a criação de uma máquina sem código interno.

**testCodigoInternoEmpty:** Verificar que não é possível a criação de uma máquina com código interno vazio.


**testNumSerieNull:** Verificar que não é possível a criação de uma máquina sem número de série.

**testNumSerieEmpty:** Verificar que não é possível a criação de uma máquina com número de série vazio.

**testDescricaoNull:** Verificar que não é possível a criação de uma máquina sem descrição.

**testDescricaoEmpty:** Verificar que não é possível a criação de uma máquina com descrição vazia.

**testMarcaNull:** Verificar que não é possível a criação de uma máquina sem marca.

**testMarcaEmpty:** Verificar que não é possível a criação de uma máquina com marca vazia.

**testModeloNull:** Verificar que não é possível a criação de uma máquina sem modelo.

**testModeloEmpty:** Verificar que não é possível a criação de uma máquina com modelo vazia.

**testDataInstalacaoNull:** Verificar que não é possível a criação de uma máquina sem data instalação.

**testDataInstalacaoMaiorQueDiaAtual:** Verificar que não é possível a criação de uma máquina com data de instalação superior que  o dia atual.


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



