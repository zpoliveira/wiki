# User Story 2004 - Especificar Ficha de Produção


# 1. Requisitos

Como Gestor de Produção, eu pretendo especificar a Ficha de Produção de um dado produto.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção terá a possibilidade de especificar uma ficha de produção a qual corresponde 
á lista de matérias-primas e respectivas quantidades usadas para produzir uma quantidade standard de um dado produto.

# 2. Análise

## Regras de Negócio

* A ficha de produção tem apenas um produto associado com quantidade e unidade do mesmo;
* Tem uma ou várias matérias-primas tal como as suas quantidades e unidades para a produção do dado produto (ComponentesMateriaPrima.class);
* O conjunto de matérias-primas como verdadeiro conjunto, não pode ter elementos repetidos;

## Testes unitários

* testQuantidadeProdutoNotNull;
* testComponenteMateriaPrimasNotNull;
* testComponenteMateriaPrimasNotEmpty;
* testQuantidadeNotNull;
* testQuantidadeIsPositive;
* testUnidadeNotNull;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  FichaProducao
*  Produto
*  ComponenteMateriaPrima
*  MateriaPrima
*  Quantidade
*  Unidade

**Controlador:**

*  EspecificarFPController
*  ListarMateriasPrimasService - (Para evitar duplicação de código)
*  ListarProdutosService - (Para evitar duplicação de código)

**Repository:**

*  ProdutoRepository
*  MateriaPrimaRepository 



## 3.1. Realização da Funcionalidade

* O Gestor de Produção inicia o processo de especificação da ficha de produção
* O sistema apresenta uma listagem dos Produtos (que não têm ficha de produção) e solicita a escolha de um (quando realizado na adição de um novo produto o sistema seleciona automáticamente o mesmo).
* O Gestor de Produção seleciona um Produto (quando não automaticamente seleccionado)
* O sistema apresenta uma listagem de matérias-primas permitindo seleccionar várias
    * Para cada matéria prima o sistema solicita a quantidade e respectiva unidade
* O Gestor de produção procede com o solicitado
* O sistema salva os dados introduzidos e informa do sucesso.  


## 3.2. SD / Diagrama de Classes

**SD**
![SDEspecificarFichaProducao](SDEspecificarFichaProducao.png)

**Diagrama de Classes**
![CDEspecificarFichaProducao.png](CDEspecificarFichaProducao.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory

## 3.4. Testes 


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*

Poderemos adicionar uma confirmação com os dados introduzidos para o Gestor verificar e posteriormente salvar.



