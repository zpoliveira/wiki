# User Story 2006 - Adiconar novo Produto
=======================================


# 1. Requisitos

Como Gestor de Produção, eu pretendo adicionar um novo Produto ao catálogo de produtos.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção terá a possibilidade de adicionar um novo produto a catálogo de produtos, correspondendo a um item que a fábrica é capaz de produzir e podendo, ou não, ter uma ficha de produçao. 

# 2. Análise

## Regras de Negócio

* Tem um codigo único de fabrico.
* Pode ou não ter uma ficha de produção.


## Testes unitários
* testeCodigoComercialNotNull
* testeCodigoComercialNotEmpty
* testeCodigoFabricoNotNull
* testeCodigoFabricoNotEmpry
* testeDescricaoBreveNotNull
* testeDescricaoBreveNotEmpty
* testeDescricaoCompletaNotNull
* testeDescricaoCompletaNotEmpty
* testProdutoUnico
* testAddProdutoDif
* testeTemNaoTemFichaProducao


# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

* Produto
* CodigoComercial
* CodigoFabricoProduto 
* DescricaoBreve
* DescricaoCompleta
* FichaProducao 

**Controlador:**

* AddProdutoUI
* EspecificarFichaProducaoUI
* AddProdutoController
* EspecificarFichaProducaoController


**Repository:**

* ProdutoRepository
* FichaProducaoRepository



## 3.1. Realização da Funcionalidade

* O Gestor de Produção solicita a especificação de um novo produto para adicionar ao catálogo de produtos
* O sistema solicita a introdução dos dados necessários
* O Gestor de Produção insere os dados
* O sistema adiciona o novo produto apresentando o sucesso
* O sistema quetiona a possibilidade de especificar ficha de produção para o produto criado
    * O gestor pretende especificar ficha produção
        * O sistema permite a especificação no caso de uso [EspecificarFichaProdução](EspecificarFP.md)
    * O gestor não pretende especificar
* O caso uso termina.


## 3.2. SD / Diagrama de Classes

**SD**
![SDAddProduto](SDAddProduto.png)

**Diagrama de Classes**
![CDAddProduto](CD_AddProduto.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Builder

## 3.4. Testes 
* testeCodigoComercialNotNull / Empty - codigo comercial nao pode ser null ou vazio
* testeCodigoFabricoNotNull / Empty - codigo unico Fabrico nao pode ser null ou vazio
* testeDescricaoBreveNotNull / Empty - descrição breve nao pode ser null ou vazia
* testeDescricaoCompletaNotNull / Empty - descrição completa nao pode ser null ou vazia
* testProdutoUnico
* testAddProdutoDif
* testeTemNaoTemFichaProducao


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

A classe de dominio Produto apresenta um atributo do tipo boleano denominado temFichaProducao, de forma a facilitar a construção das queries assim como tem em vista a performance.

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

* Se o Gestor de Produção quiser especificar um produto como matéria-prima tem de, quando essa necessidade surgir, iniciar o caso de uso de especificar matéria-prima.


