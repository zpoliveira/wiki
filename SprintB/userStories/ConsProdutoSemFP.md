# User Story 2003 - Consultar Produtos sem Ficha de Produção
=======================================


# 1. Requisitos

Como Gestor de Produção, eu quero consultar os produtos que não têm Ficha de Produção definida.

A interpretação feita deste requisito foi no sentido de que o Gestor de Produção terá a possibilidade de listar todos os produtos que ainda não tenham ficha de produção definida.

# 2. Análise

## Regras de Negócio

* Apenas produtos sem ficha de produção devem ser listados, mantendo-se as mesmas regras das entidades de dominio usadas.

## Testes unitários



# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

*  Produto

**Controlador:**

*  ListarProdutosSemFPUI
*  ListarProdutosService - (Para evitar duplicação de código)


**Repository:**

*  ProdutoRepository



## 3.1. Realização da Funcionalidade

* O Gestor de Produção solicita a listagem de produtos sem ficha de produção
* O sistema apresenta uma listagem dos Produtos sem ficha de produção.


## 3.2. SD / Diagrama de Classes

**SD**
![SDListarProdutosSemFP](SDListarProdutos.png)


## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Visitor

## 3.4. Testes 



# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



