# User Story 3002 - Especificar Nova Linha de Produção
=======================================


# 1. Requisitos

Como Gestor de Chão de Fabrica, eu pretendo registar uma nova linha produção.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fabrica teria a possibilidade de acrescentar uma nova linha de produção ao conjunto de depósitos existentes no sistema. 

# 2. Análise

## Regras de Negócio

* O Código tem de ser único no sistema;


## Testes unitários

* testCodigoNull;
* testCodigoEmpty;
* testMaquinasNull;
* testMaquinasEmpty;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

* LinhaProducao
* LinhaProducaoIdentificacao
* SequenciaMaquina

**Controlador:**

* EspecificarNovaLinhaProducaoController


**Repository:**

* LinhaProducaoRepository 


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fabrica inicia o processo de inserção de registo da linha produção
* O sistema solicita o Código
* O sistema solicita a seleção das maquinas pertencentes á linha de produção
* O sistema apresenta os dados introduzidos e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SD_DefLinhaProducao](SD_DefLinhaProducao.png)

**Diagrama de Classes**
![CD_DefLinhaProducao](CD_DefLinhaProducao.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

**testCodigoNull:** Verificar que não é possível a criação da linha de produção sem código.

**testCodigoEmpty:** Verificar que não é possível a criação de uma linha produção com código vazio.

**testDescricaoNull:** Verificar que não é possível a criação de uma linha produção sem máquinas.

**testDescricaoEmpty:** Verificar que não é possível a criação de uma linha produção com a lista de máquinas vazia.


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*