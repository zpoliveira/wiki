# User Story 1009 - Especificar Novo Depósito
=======================================


# 1. Requisitos

Como Gestor de Chão de Fabrica, eu pretendo registar um novo depósito.

A interpretação feita deste requisito foi no sentido de que o Gestor de Chão de Fabrica teria a possibilidade de acrescentar uma nova máquina ao conjunto de depósitos existentes no sistema. 

# 2. Análise

## Regras de Negócio

* O Código Alfanumérico tem de ser único no sistema;
* O Códifo Alfanumérico como indica tem de alfanumérico; 


## Testes unitários

* testCodigoNull;
* testCodigoEmpty;
* testCodigoNotAlfaNumeric;
* testDescricaoNull;
* testDescricaoEmpty;

# 3. Design

* Utilizar a estrutura base standard da aplicação baseada em camadas

**Classes do domínio:**

* Deposito
* CodigoAlfanumerico
* DescricaoDeposito

**Controlador:**

* EspecificarNovoDepositoController


**Repository:**

* DepositoRepository 


## 3.1. Realização da Funcionalidade

* O Gestor de Chão de Fabrica inicia o processo de inserção de registo do Depósito
* O sistema solicita os dados necessários Código, Descrição
* O sistema apresenta os dados introduzidos e mensagem de sucesso   


## 3.2. SD / Diagrama de Classes

**SD**
![SD_EspDeposito](SD_EspDeposito.png)

**Diagrama de Classes**
![CD_EspDeposito](CD_EspDeposito.png)

## 3.3. Padrões Aplicados

* Padrão Repository
* Padrão Factory
* Padrão Command (no menu de bootstrap)

## 3.4. Testes 

**testCodigoNull:** Verificar que não é possível a criação de um deposito sem código.

**testCodigoEmpty:** Verificar que não é possível a criação de um depósito com código vazio.


**testCodigoNotAlfaNumeric:** Verificar que não é possível a criação de um depósito com código com caracteres que não os alfanumericos.

**testNumSerieEmpty:** Verificar que não é possível a criação de uma máquina com número de série vazio.

**testDescricaoNull:** Verificar que não é possível a criação de um depósito sem descrição.

**testDescricaoEmpty:** Verificar que não é possível a criação de um depósito com descrição vazia.


# 4. Implementação

*Nesta secção a equipa deve providenciar, se necessário, algumas evidências de que a implementação está em conformidade com o design efetuado. Para além disso, deve mencionar/descrever a existência de outros ficheiros (e.g. de configuração) relevantes e destacar commits relevantes;*

*Recomenda-se que organize este conteúdo por subsecções.*

# 5. Integração/Demonstração

*Nesta secção a equipa deve descrever os esforços realizados no sentido de integrar a funcionalidade desenvolvida com as restantes funcionalidades do sistema.*

# 6. Observações

*Nesta secção sugere-se que a equipa apresente uma perspetiva critica sobre o trabalho desenvolvido apontando, por exemplo, outras alternativas e ou trabalhos futuros relacionados.*



