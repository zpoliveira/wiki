# Projeto Integrador da LEI-ISEP Sem4 2019-20 SprintB

## User Stories

### Produção

- [User Story 1004 - Boot Matérias Primas](./userStories/BootMatPrima.md)
- [User Story 2001 - Adicionar Matéria Prima](./userStories/AddMatPrima.md)
- [User Story 1005 - Boot Categorias Matérias Primas](./userStories/BootCatMatPrima.md)
- [User Story 2002 - Definir Nova Categoria Matéria Prima](./userStories/DefinCatMatPrima.md)
- [User Story 1006 - Boot Produtos](./userStories/BootProdutos.md)
- [User Story 2003 - Consulta Produtos sem Ficha Produção](./userStories/ConsProdutoSemFP.md)
- [User Story 2004 - Especificar Ficha de Produção](./userStories/EspecificarFP.md)
- [User Story 2005 - Importar Produtos CSV](./userStories/ImportProdutosCSV.md)
- [User Story 2006 - Adicionar Produto](./userStories/AddProduto.md)

### Chão de Fábrica

- [User Story 1007 - Boot Maquinas](./userStories/BootMaquina.md)
- [User Story 3001 - Definir Nova Maquina](./userStories/DefinMaquina.md)
- [User Story 1008 - Boot Linhas Produção](./userStories/BootLinhaProd.md)
- [User Story 3002 - Especificar Nova Linha Produção](./userStories/EspecificarLinhaProd.md)
- [User Story 1009 - Boot Depósitos](./userStories/BootDeposito.md)
- [User Story 3003 - Especificar Novo Depósitos](./userStories/EspecificarDeposit.md)
